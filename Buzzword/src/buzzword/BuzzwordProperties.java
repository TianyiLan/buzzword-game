package buzzword;

/**
 * @author Ritwik Banerjee, Tianyi Lan
 */
public enum BuzzwordProperties {
    //css properties
    BOTTOM_TOOLBAR,

    //numeric value
    USER_INFO_LENGTH,
    NUMBER_LEVELS_IN_EACH_MODE,
    NUMBER_GAME_MODES,
    BASE_TIME,
    BASE_TARGET_SCORE,

    //file name
    USER_INFO_FILE_NAME,

    //heading
    WORKSPACE_HEADING,
    USER_INFO_DIALOG_HEADING,
    CHANGE_DIALOG_HEADING,
    LOCK_HEADING,
    UNLOCK_HEADING,

    //icon and image
    BUZZWORD_ICON,
    CHECK_IMAGE,
    DELETE_IMAGE,
    LEVEL_IMAGE,
    LOGIN_IMAGE,
    LOGOUT_IMAGE,
    MENU_IMAGE,
    PAUSE_IMAGE,
    PLAY_IMAGE,
    PROFILE_IMAGE,
    REPLAY_IMAGE,
    CANCEL_IMAGE,
    OK_IMAGE,
    RESUME_IMAGE,
    CHANGE_IMAGE,
    NEXT_IMAGE,
    HELP_IMAGE,
    QUESTION_IMAGE,
    PLACES_IMAGE,
    CELEBRITY_IMAGE,
    DICTIONARY_IMAGE,
    ANIMAL_IMAGE,
    COMMON_IMAGE,
    INFO_IMAGE,
    FAIL_IMAGE,

    //tooltip for buttons
    DELETE_TOOLTIPS,
    HELP_TOOLTIPS,
    LEVEL_TOOLTIPS,
    LOGIN_TOOLTIPS,
    LOGOUT_TOOLTIPS,
    MENU_TOOLTIPS,
    PAUSE_TOOLTIPS,
    PLAY_TOOLTIPS,
    PROFILE_TOOLTIPS,
    CHANGE_TOOLTIPS,
    REPLAY_TOOLTIPS,
    CANCEL_TOOLTIPS,
    OK_TOOLTIPS,
    CLOSE_TOOLTIPS,
    RESUME_TOOLTIPS,
    NEXT_TOOLTIPS,
    GAMEMODE_PLACES_TOOLTIPS,
    GAMEMODE_DICTIONARY_TOOLTIPS,
    GAMEMODE_NAME_TOOLTIPS,
    GAMEMODE_COMMON_TOOLTIPS,

    //title
    LOGIN_TITLE,
    PROFILE_TITLE,
    CONFIRMATION_TITLE,
    HELP_TITLE,
    CHANGE_TITLE,

    //message
    CHANGE_MESSAGE_CHECK,
    CHANGE_MESSAGE_TIP,
    LOGIN_MESSAGE_CHECK,
    LOGIN_MESSAGE_NOT_MATCH,
    PROFILE_MESSAGE_CHECK,
    PROFILE_MESSAGE_TIP,
    PROFILE_MESSAGE_NAME_EXIST,
    HELP_MESSAGE,
    CONFIRMATION_EXIT,
    CONFIRMATION_LOGOUT,
    CONFIRMATION_PROFILE,
    CONFIRMATION_DELETE,
    CONFIRMATION_REPLAY,
}
