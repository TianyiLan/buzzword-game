package gui;

import java.io.IOException;

import data.GameData;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import propertymanager.PropertyManager;
import ui.AppGUI;

import static buzzword.BuzzwordProperties.*;

/**
 * This class is the dialog model for users to input their information.
 *
 * @author Tianyi Lan
 */
public class UserInfoDialog {

    //two types of dialog
    public enum DialogType {
        PROFILE_CREATION,
        LOGIN,
    }

    private DialogType type;
    private Dialog<?> dialog;
    private boolean usernameAlreadyExists;
    private boolean loginFailed;
    private boolean success;
    private GameData gamedata;

    public UserInfoDialog(DialogType type, GameData data) {
        this.type = type;
        this.gamedata = data;
        usernameAlreadyExists = false;
        loginFailed = false;
        success = false;
    }

    public DialogType getDialogType() {
        return this.type;
    }

    public void close(){
        dialog.hide();
    }
    public boolean isShowing(){return dialog.isShowing();}

    public boolean show() throws IOException {
        dialog = new Dialog<>();
        PropertyManager propertyManager = PropertyManager.getManager();
        ButtonType ok;
        ButtonType cancel;

        //set icon and image for profile dialog
        if (type == DialogType.PROFILE_CREATION) {
            decProfileDialog(propertyManager);
        }

        //set icon and image for login dialog
        if (type == DialogType.LOGIN) {
            decLoginDialog(propertyManager);
        }

        //set the button types
        ok = new ButtonType(null);
        cancel = new ButtonType(null);
        dialog.getDialogPane().getButtonTypes().addAll(ok, cancel);
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ok);
        Workspace.decButton(okButton, OK_IMAGE.toString(), OK_TOOLTIPS.toString());
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(cancel);
        Workspace.decButton(cancelButton, CANCEL_IMAGE.toString(), CANCEL_TOOLTIPS.toString());
        setCloseButtonX();

        //tips and messages
        HBox mPane = new HBox();
        Label message = new Label();
        message.setVisible(false);
        mPane.getChildren().add(message);
        if (type == DialogType.PROFILE_CREATION) {
            message.setText(propertyManager.getPropertyValue(PROFILE_MESSAGE_TIP));
            message.setVisible(true);
            message.setTextFill(Color.BLACK);
        }

        //create the username label and field
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        //show message if username already exists
        if (usernameAlreadyExists) {
            message.setText(propertyManager.getPropertyValue(PROFILE_MESSAGE_NAME_EXIST));
            message.setVisible(true);
            message.setTextFill(Color.RED);
        }

        //show message if login failed
        if(loginFailed){
            message.setText(propertyManager.getPropertyValue(LOGIN_MESSAGE_NOT_MATCH));
            message.setVisible(true);
            message.setTextFill(Color.RED);
        }

        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");


        //a dialog needs both username and password text fields
        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);
        grid.add(mPane, 0, 2);
        GridPane.setColumnSpan(mPane, 3);
        dialog.getDialogPane().setContent(grid);


        //enable/disable login button depending on whether a username/password was entered.
        //use boolean binding for enabling button only if all text fields have text
        //note that keyword "or" means as long as one condition is false, the result is false
        if(type == DialogType.LOGIN) {
            BooleanBinding bb = username.textProperty().isEmpty()
                    .or(password.textProperty().isEmpty());
            okButton.disableProperty().bind(bb);
        }
        //for profile dialog, check if both fields have at least 5 characters
        if(type == DialogType.PROFILE_CREATION) {
            int minLength = Integer.valueOf(propertyManager.getPropertyValue(USER_INFO_LENGTH));
            BooleanBinding bb = username.textProperty().length().lessThan(minLength)
                    .or(password.textProperty().length().lessThan(minLength));
            okButton.disableProperty().bind(bb);
        }

        //request focus on username field
        Platform.runLater((username::requestFocus));

        //player's response
        dialog.showAndWait().ifPresent(response -> {
            if(response == cancel) {
                dialog.close();
            }
            if(response == ok){
                if(type == DialogType.LOGIN){
                    try {
                        handleLoginResponse(username.getText(), password.getText());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(type == DialogType.PROFILE_CREATION){
                    try {
                        handleProfileResponse(username.getText(), password.getText());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        return success;
    }//show method ends


    //handle login
    private void handleLoginResponse(String username, String pw) throws IOException {
        //first, check if user info match
        if(!gamedata.login(username, pw)){
            //login failed
            loginFailed = true;
        }
        else{
            success = true;
        }

        //success
        if (success) {
            MessageDialog check = new MessageDialog();
            check.setImage(BUZZWORD_ICON.toString(), CHECK_IMAGE.toString());
            check.show(LOGIN_TITLE.toString(), LOGIN_MESSAGE_CHECK.toString());
        }
        if(loginFailed && !success){
            show();
        }
    }

    //handle profile creation
    private void handleProfileResponse(String username, String pw) throws IOException {
        if (!gamedata.createNewProfile(username, pw)) {
            usernameAlreadyExists = true;
        }
        else {
            success = true;
        }

        //success
        if (success) {
            MessageDialog check = new MessageDialog();
            check.setImage(BUZZWORD_ICON.toString(), CHECK_IMAGE.toString());
            check.show(PROFILE_TITLE.toString(), PROFILE_MESSAGE_CHECK.toString());
        }
        if(usernameAlreadyExists && !success){
            show();
        }
    }

    private void decLoginDialog(PropertyManager propertyManager) throws IOException {
        //the custom dialog
        dialog.setTitle(propertyManager.getPropertyValue(LOGIN_TITLE));
        dialog.setHeaderText(propertyManager.getPropertyValue(USER_INFO_DIALOG_HEADING));

        //set icon
        Image icon = (AppGUI.initializeImageView(BUZZWORD_ICON.toString()).getImage());
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        //set image
        dialog.setGraphic(AppGUI.initializeImageView(LOGIN_IMAGE.toString()));
    }
    private void decProfileDialog(PropertyManager propertyManager) throws IOException {
        //the custom dialog
        dialog.setTitle(propertyManager.getPropertyValue(PROFILE_TITLE));
        dialog.setHeaderText(propertyManager.getPropertyValue(USER_INFO_DIALOG_HEADING));

        //set icon
        Image icon = (AppGUI.initializeImageView(BUZZWORD_ICON.toString()).getImage());
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        //set image
        dialog.setGraphic(AppGUI.initializeImageView(PROFILE_IMAGE.toString()));
    }

    //close the dialog when clicking the close button X
    private void  setCloseButtonX(){
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> window.hide());
    }
}

