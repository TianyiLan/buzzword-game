package gui;

import javafx.scene.layout.StackPane;
import javafx.scene.effect.Light.Point;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Graph graphical component 1: Vertex.
 * A graph contains vertices and edges.
 * Vertices, also known as nodes, are points in a graph, connecting (or not) by edges.
 * Note that in this app, the graphic shell of the vertex is a square
 *
 * @author Tianyi Lan
 */
public class Vertex extends StackPane {

    public static final Color HIGHLIGHT_COLOR = Color.ORANGERED;
    public static final Color DEFAULT_STROKE_COLOR = Color.TRANSPARENT;
    private static final Color HIGHLIGHT_LETTER_COLOR = Color.WHEAT;
    private static final Color DEFAULT_LETTER_COLOR = Color.BLACK;
    private static final Color DEFAULT_BACKGROUND_COLOR = Color.ORANGE;
    private static int DEFAULT_WIDTH = 50;

    private int id;
    private Text letter;
    private boolean marked;
    private boolean highlighted;
    private boolean isPathHead;
    private HashSet<Vertex> neighbours;
    private HashSet<Edge> edges;

    private Point top;
    private Point bottom;
    private Point left;
    private Point right;

    /**
     * Default constructor
     *
     * @param x The x coordinates of the vertex
     * @param y The y coordinates of the vertex
     * @param size The width of the square
     * @param id The id of the vertex
     * @param letter The string value contained in the vertex
     */
    public Vertex(double x, double y, double size, int id, String letter) {
        super();
        Rectangle box = new Rectangle(size, size);
        super.setPrefSize(size, size);
        setLayoutX(x);
        setLayoutY(y);
        this.id = id;
        marked = false;
        highlighted = false;
        isPathHead = false;
        this.letter = new Text(letter.toUpperCase());
        this.letter.setFont(Font.font("Arial Black", 40));
        box.setFill(DEFAULT_BACKGROUND_COLOR);
        box.setStrokeWidth(2);
        box.setStroke(Color.TRANSPARENT);
        neighbours = new HashSet<>();
        edges = new HashSet<>();
        super.getChildren().addAll(box, this.letter);
        setPoints();
    }

    /**
     * Overloaded constructor with some default values. Note that this
     * constructor will be mainly used for this app.
     *
     * @param x The x coordinates of the vertex
     * @param y The y coordinates of the vertex
     * @param id The id of the vertex
     */
    public Vertex(double x, double y, int id) {
        this(x, y, DEFAULT_WIDTH, id, "");
    }

    /**
     * Sets the vertex highlighted or not.
     *
     * @param state Whether or not the vertex is highlighted
     */
    public void setHighlight(boolean state) {
        if (highlighted != state) {
            highlighted = state;
            if (state) {
                ((Rectangle) this.getChildren().get(0)).setFill(HIGHLIGHT_COLOR);
                letter.setFill(HIGHLIGHT_LETTER_COLOR);
            } else {
                ((Rectangle) this.getChildren().get(0)).setFill(DEFAULT_BACKGROUND_COLOR);
                letter.setFill(DEFAULT_LETTER_COLOR);
            }
        }
    }

    /**
     * Sets this vertex's neighbours automatically.
     * Note that this method should be called iff ALL EDGES ARE ALREADY CONNECTED.
     */
    public void setNeighbours(){
        for(Edge edge: edges){
            if(edge.getFrom() != this) {
                neighbours.add(edge.getFrom());
            }
            if(edge.getTo() != this) {
                neighbours.add(edge.getTo());
            }
        }
    }

    /**
     * Manually adds all neighbours.
     *
     * @param neighbour All the neighbours of this vertex to be added
     */
    public void addAllNeighbours(Vertex... neighbour) {neighbours.addAll(Arrays.asList(neighbour));}

    /**
     * Manually adds a neighbour
     *
     * @param neighbour A neighbour of this vertex to be added
     */
    public void addNeighbour(Vertex neighbour) {neighbours.add(neighbour);}

    /**
     * Adds an edge
     *
     * @param edge An edge linked to this vertex to be added
     */
    public void addEdge(Edge edge){edges.add(edge);}


    public boolean isReachableFromPathHead(){
        for(Vertex neighbour: neighbours){
            if(neighbour.isPathHead()){
                return true;
            }
        }
        return false;
    }

    public List<Vertex> getAvailableNeighbourWith(List<Vertex> path, String letter){
        List<Vertex> temp = new ArrayList<>();
        for(Vertex v: neighbours){
            if(v.contains(letter) && !path.contains(v)){
                temp.add(v);
            }
        }
        return temp;
    }

    public boolean contains(String letter){
        if(letter == null){
            return false;
        }
        else{
            return this.letter.getText().toLowerCase().equals(letter);
        }
    }
    public boolean contains(char letter){
        if(letter == 0){
            return false;
        }
        else {
            return contains(String.valueOf(letter));
        }
    }


    public Edge getCommonEdge(Vertex target){
        for(Edge e: edges){
            if(e.getFrom() == target && e.getTo() == this){
                return e;
            }
            else if(e.getFrom() == this && e.getTo() == target){
                return e;
            }
        }
        return null;
    }

    public void highlightPathToHead(){
        neighbours.stream().filter(Vertex::isPathHead).forEach(neighbour -> {
            Edge common = getCommonEdge(neighbour);
            common.setHighlight(true);
            neighbour.setPathHead(false);
            setPathHead(true);
        });
    }

    public void unhighlightLinkedEdges(){
        for(Edge e: edges){
            e.setHighlight(false);
        }
    }


    public int getID(){return id;}
    public String getLetter(){return letter.getText().toLowerCase();}
    public char getChar(){return letter.getText().toLowerCase().charAt(0);}
    public void setLetter(String letter){this.letter.setText(letter);}
    public boolean isMarked(){return marked;}
    public void setMarked(boolean state){marked = state;}
    public boolean isHighlighted(){return highlighted;}
    public void setPathHead(boolean isPathHead){this.isPathHead = isPathHead;}
    public boolean isPathHead(){return isPathHead;}
    public HashSet<Edge> getEdges(){return edges;}
    public HashSet<Vertex> getNeighbours(){return neighbours;}
    public Point getTop(){return top;}
    public Point getBottom(){return bottom;}
    public Point getLeft(){return left;}
    public Point getRight(){return right;}

    /**
     * Sets the top, bottom, left and right points for the vertex ready to be linked
     */
    private void setPoints() {
        top = new Point();
        bottom = new Point();
        left = new Point();
        right = new Point();
        top.setX(getLayoutX() + getPrefWidth() / 2 + 2);
        top.setY(getLayoutY() + 2);
        bottom.setX(getLayoutX() + getPrefWidth() / 2 + 2);
        bottom.setY(getLayoutY() + getPrefWidth() + 5);
        left.setX(getLayoutX());
        left.setY(getLayoutY() + getPrefWidth() / 2 + 3);
        right.setX(getLayoutX() + getPrefWidth() + 3);
        right.setY(getLayoutY() + getPrefWidth() / 2 + 3);
    }
}
