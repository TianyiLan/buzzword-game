package gui;

import data.GameData;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static buzzword.BuzzwordProperties.*;

/**
 * This class is the dialog model for users to change their password.
 *
 * @author Tianyi Lan
 */
public class ChangePWDialog {

    private Dialog<?> dialog;
    private GameData gamedata;
    private boolean success;

    public ChangePWDialog(GameData data) {
        this.gamedata = data;
        success = false;
    }

    public void close(){
        dialog.hide();
    }
    public boolean isShowing(){return dialog.isShowing();}

    public boolean show() throws IOException {
        dialog = new Dialog<>();
        PropertyManager propertyManager = PropertyManager.getManager();
        ButtonType ok;
        ButtonType cancel;

        //set icon and image for dialog
        dialog.setTitle(propertyManager.getPropertyValue(CHANGE_TITLE));
        dialog.setHeaderText(propertyManager.getPropertyValue(CHANGE_DIALOG_HEADING));

        //set icon
        Image icon = (AppGUI.initializeImageView(BUZZWORD_ICON.toString()).getImage());
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        //set image
        dialog.setGraphic(AppGUI.initializeImageView(CHANGE_IMAGE.toString()));


        //set the button types
        ok = new ButtonType(null);
        cancel = new ButtonType(null);
        dialog.getDialogPane().getButtonTypes().addAll(ok, cancel);
        Button okButton = (Button) dialog.getDialogPane().lookupButton(ok);
        Workspace.decButton(okButton, OK_IMAGE.toString(), OK_TOOLTIPS.toString());
        Button cancelButton = (Button) dialog.getDialogPane().lookupButton(cancel);
        Workspace.decButton(cancelButton, CANCEL_IMAGE.toString(), CANCEL_TOOLTIPS.toString());
        setCloseButtonX();

        //tips and messages
        HBox mPane = new HBox();
        Label message = new Label();
        message.setVisible(true);
        message.setText(propertyManager.getPropertyValue(CHANGE_MESSAGE_TIP));
        mPane.getChildren().add(message);


        //create the username label and field
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        PasswordField password = new PasswordField();
        password.setPromptText("Password");


        //a dialog needs a password text field
        grid.add(new Label("Password:"), 0, 0);
        grid.add(password, 1, 0);
        grid.add(mPane, 0, 1);
        GridPane.setColumnSpan(mPane, 3);
        dialog.getDialogPane().setContent(grid);


        //enable/disable login button depending on whether a username/password was entered.
        //use boolean binding for enabling button only if all text fields have text
        //note that keyword "or" means as long as one condition is false, the result is false
        int minLength = Integer.valueOf(propertyManager.getPropertyValue(USER_INFO_LENGTH));
        BooleanBinding bb = password.textProperty().length().lessThan(minLength);
        okButton.disableProperty().bind(bb);


        //request focus on username field
        Platform.runLater((password::requestFocus));

        //player's response
        dialog.showAndWait().ifPresent(response -> {
            if(response == cancel) {
                dialog.close();
            }
            if(response == ok){
                try {
                    handleChangeResponse(password.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return success;
    }

    private void handleChangeResponse(String pw) throws IOException {
        success = true;
        gamedata.changePassword(pw);
        MessageDialog check = new MessageDialog();
        check.setImage(BUZZWORD_ICON.toString(), CHECK_IMAGE.toString());
        check.show(CHANGE_TITLE.toString(), CHANGE_MESSAGE_CHECK.toString());

    }

    //close the dialog when clicking the close button X
    private void  setCloseButtonX(){
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> window.hide());
    }
}
