package gui;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.Window;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static buzzword.BuzzwordProperties.CANCEL_IMAGE;
import static buzzword.BuzzwordProperties.CLOSE_TOOLTIPS;


/**
 * This class is the message dialog model for displaying messages.
 *
 * @author Tianyi Lan
 */
public class MessageDialog {

    private Alert dialog;

    public MessageDialog() {
        dialog = new Alert(Alert.AlertType.INFORMATION);
    }

    private void setMessage(String message) {
        PropertyManager propertyManager = PropertyManager.getManager();
        dialog.setContentText(propertyManager.getPropertyValue(message));
    }

    private void setTitle(String title) {
        PropertyManager propertyManager = PropertyManager.getManager();
        dialog.setTitle(propertyManager.getPropertyValue(title));
    }

    private void setHeader(String header) {
        dialog.setHeaderText(header);
    }

    public void setImage(String iconFileName, String imageFileName) throws IOException {
        //set icon for this dialog
        Image icon = (AppGUI.initializeImageView(iconFileName).getImage());
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        //set image
        dialog.setGraphic(AppGUI.initializeImageView(imageFileName));
    }

    public void close(){
        dialog.hide();
    }
    public boolean isShowing(){
        return dialog.isShowing();
    }

    public void showMSG(String title, String message) throws IOException {
        showMSG(title, null, message);
    }

    public void showMSG(String title, String header, String message) throws IOException {
        dialog.setTitle(title);
        setHeader(header);
        dialog.setContentText(message);
        ButtonType close = new ButtonType(null);
        dialog.getButtonTypes().setAll(close);
        Button closeButton = (Button) dialog.getDialogPane().lookupButton(close);
        Workspace.decButton(closeButton, CANCEL_IMAGE.toString(), CLOSE_TOOLTIPS.toString());
        setCloseButtonX();

        dialog.show();
    }

    public void show(String title, String message) throws IOException {
        setTitle(title);
        setHeader(null);
        setMessage(message);
        ButtonType close = new ButtonType(null);
        dialog.getButtonTypes().setAll(close);
        Button closeButton = (Button) dialog.getDialogPane().lookupButton(close);
        Workspace.decButton(closeButton, CANCEL_IMAGE.toString(), CLOSE_TOOLTIPS.toString());
        setCloseButtonX();

        dialog.showAndWait().ifPresent(response -> {
            if(response == close) {
                dialog.close();
            }
        });
    }

    //close the dialog when clicking the close button X
    private void  setCloseButtonX(){
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> window.hide());
    }
}