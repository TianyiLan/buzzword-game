package gui;

import data.GameData;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static buzzword.BuzzwordProperties.*;

/**
 * This class is for rendering the level selection screen with all game modes and all levels
 *
 * @author Tianyi Lan
 */
public class LevelSelectionScreen extends BorderPane {

    private static String gameModeSelected;
    private static int levelSelected;          //start at 0
    private static boolean isLevelSelected;

    private Label personalBestLabel;
    private Label wordsLabel;
    private static Label wordsNum;
    private Label scoreLabel;
    private static Label totalScore;

    private int levelNumPerGameMode;
    private int gameModeTotalNum;
    private GameData gameData;
    private Workspace workspace;

    private String[] gameModesNames;
    private ImageView[] gameModesImages;
    private Label[] gameModeLabels;
    private LevelBox animal;
    private LevelBox places;
    private LevelBox celebrity;
    private LevelBox dictionary;
    private List<LevelBox> allGameModes;
    private List<Map<Integer, GameData.GameProgressList>> allGameProgressMaps;

    public LevelSelectionScreen(GameData gameData, Workspace workspace) throws IOException {
        this.gameData = gameData;
        this.workspace = workspace;
        isLevelSelected = false;
        loadGameModes();
        loadLevels();
        init();
    }

    private void loadGameModes() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        gameModeTotalNum = Integer.valueOf(propertyManager.getPropertyValue(NUMBER_GAME_MODES));
        gameModesNames = new String[]{propertyManager.getPropertyValue(GAMEMODE_COMMON_TOOLTIPS),
                propertyManager.getPropertyValue(GAMEMODE_PLACES_TOOLTIPS),
                propertyManager.getPropertyValue(GAMEMODE_NAME_TOOLTIPS),
                propertyManager.getPropertyValue(GAMEMODE_DICTIONARY_TOOLTIPS)
        };
        gameModesImages = new ImageView[]{AppGUI.initializeImageView(COMMON_IMAGE.toString()),
                AppGUI.initializeImageView(PLACES_IMAGE.toString()),
                AppGUI.initializeImageView(CELEBRITY_IMAGE.toString()),
                AppGUI.initializeImageView(DICTIONARY_IMAGE.toString()),
        };
    }

    private void loadLevels(){
        PropertyManager propertyManager = PropertyManager.getManager();
        levelNumPerGameMode = Integer.valueOf(propertyManager.getPropertyValue(NUMBER_LEVELS_IN_EACH_MODE));

        //load levels in all modes
        allGameModes = new ArrayList<>();
        allGameProgressMaps = new ArrayList<>();

        for(int i = 0; i < gameModeTotalNum; i++){
            Text[] letters = new Text[levelNumPerGameMode];
            for(int j = 0; j < levelNumPerGameMode; j++){
                letters[j] = new Text(String.valueOf(j+1));
                letters[j].setFont(Font.font("Arial Black", 45));
            }
            LevelBox temp = new LevelBox(letters, 60);
            temp.setBox(Color.ORANGE, Color.WHITE, 1.5);
            temp.setTextFill(Color.WHITE);
            if(i == 0){
                animal = temp;
                allGameModes.add(animal);
                allGameProgressMaps.add(gameData.getCommonProgressMap());
            }
            if(i == 1){
                places = temp;
                allGameModes.add(places);
                allGameProgressMaps.add(gameData.getPlacesProgressMap());
            }
            if(i == 2){
                celebrity =temp;
                allGameModes.add(celebrity);
                allGameProgressMaps.add(gameData.getCelebrityProgressMap());
            }
            if(i == 3){
                dictionary = temp;
                allGameModes.add(dictionary);
                allGameProgressMaps.add(gameData.getDictionaryWordsProgressMap());
            }
        }
        loadGameProgress();
    }

    //load game progress(whether a certain level is cleared)
    @SuppressWarnings("Duplicates")
    private void loadGameProgress(){
        for(int level = 1; level < levelNumPerGameMode+1; level++){
            if(level == 1){
                boolean isCleared = gameData.getIsCleared(gameData.getCommonProgressMap(), level);
                animal.markBox(level-1, isCleared);
                isCleared = gameData.getIsCleared(gameData.getPlacesProgressMap(), level);
                places.markBox(level-1, isCleared);
                isCleared = gameData.getIsCleared(gameData.getCelebrityProgressMap(), level);
                celebrity.markBox(level-1, isCleared);
                isCleared = gameData.getIsCleared(gameData.getDictionaryWordsProgressMap(), level);
                dictionary.markBox(level-1, isCleared);
            }
            //level >= 2
            else{
                boolean isCleared = gameData.getIsCleared(gameData.getCommonProgressMap(), level);
                animal.markBox(level-1, isCleared);
                isCleared = gameData.getIsCleared(gameData.getPlacesProgressMap(), level);
                places.markBox(level-1, isCleared);
                isCleared = gameData.getIsCleared(gameData.getCelebrityProgressMap(), level);
                celebrity.markBox(level-1, isCleared);
                isCleared = gameData.getIsCleared(gameData.getDictionaryWordsProgressMap(), level);
                dictionary.markBox(level-1, isCleared);
                //if the prev level is cleared, then the current level is playable(not disable), otherwise, it's not
                boolean prevLevelIsCleared;
                prevLevelIsCleared = gameData.getIsCleared(gameData.getCommonProgressMap(), level-1);
                animal.setBoxDisable(level-1, !prevLevelIsCleared);
                prevLevelIsCleared = gameData.getIsCleared(gameData.getPlacesProgressMap(), level-1);
                places.setBoxDisable(level-1, !prevLevelIsCleared);
                prevLevelIsCleared = gameData.getIsCleared(gameData.getCelebrityProgressMap(), level-1);
                celebrity.setBoxDisable(level-1, !prevLevelIsCleared);
                prevLevelIsCleared = gameData.getIsCleared(gameData.getDictionaryWordsProgressMap(), level-1);
                dictionary.setBoxDisable(level-1, !prevLevelIsCleared);
            }
        }
    }

    private void init() throws IOException {
        VBox modesPane = new VBox();
        modesPane.setAlignment(Pos.TOP_CENTER);
        modesPane.setSpacing(15);
        modesPane.setPadding(new Insets(5,5,5,5));
        //game mode heading
        Text gameModeHeading = new Text("Game Mode");
        DropShadow ds = new DropShadow();
        ds.setOffsetY(2.0f);
        ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
        gameModeHeading.setFont(new Font("Arial Black", 15));
        gameModeHeading.setStyle("-fx-fill: orangered");
        gameModeHeading.setEffect(ds);
        modesPane.getChildren().add(gameModeHeading);
        //labels to display game mode
        gameModeLabels = new Label[gameModesNames.length];
        for(int i = 0; i< gameModesNames.length; i++){
            gameModeLabels[i] = new Label();
            gameModeLabels[i].setGraphic(gameModesImages[i]);
            gameModeLabels[i].setTooltip(new Tooltip(gameModesNames[i]));
            modesPane.getChildren().add(gameModeLabels[i]);
        }
        //set left to game mode selection
        setLeft(modesPane);

        //pane for rendering levels and progress
        VBox levelsAndProgressPane = new VBox();
        levelsAndProgressPane.setAlignment(Pos.CENTER_LEFT);
        levelsAndProgressPane.setPadding(new Insets(0,0,0,200));
        levelsAndProgressPane.setSpacing(10);
        //set up handler
        setupGameModesLabelHandler(levelsAndProgressPane, gameModeLabels);

        //set center to level and progress pane
        setCenter(levelsAndProgressPane);
    }

    private void setupGameModesLabelHandler(VBox root, Label[] gameModeLabels){
        for(Label label: gameModeLabels){
            label.setStyle("-fx-border-color: transparent");
            label.setOnMouseEntered(e -> label.setStyle("-fx-border-color: orange"));
            label.setOnMouseExited(e -> label.setStyle("-fx-border-color: transparent"));
            label.setOnMouseClicked(e ->{
                for(Label l: gameModeLabels){
                    if(l.isDisable()){
                        isLevelSelected = false;
                        workspace.getStartGameButton().setDisable(!isLevelSelected);
                        restoreLevelsBackgroundColor(l, label, levelSelected);
                        l.setDisable(false);
                    }
                }
                label.setDisable(true);
                showLevelGrid(root, label.getTooltip().getText());
            });
        }
    }

    private void showLevelGrid(VBox root, String gameMode){
        PropertyManager propertyManager = PropertyManager.getManager();
        GridPane pane = new GridPane();
        LevelBox temp = null;
        int levelNum = Integer.valueOf(propertyManager.getPropertyValue(NUMBER_LEVELS_IN_EACH_MODE));
        //animal game mode
        if(gameMode.equals(gameModesNames[0])) {
                    temp = animal;
        }
        //places game mode
        if(gameMode.equals(gameModesNames[1])) {
            temp = places;
        }
        //celebrity game mode
        if(gameMode.equals(gameModesNames[2])) {
            temp = celebrity;
        }
        //dictionary game mode
        if(gameMode.equals(gameModesNames[3])) {
            temp = dictionary;
        }

        pane.setAlignment(Pos.CENTER_LEFT);
        pane.setHgap(1);
        pane.setVgap(1);
        assert temp != null;
        for(int i = 0; i < temp.size(); i++) {
            setLevelGridOnMouseInteraction(gameMode, temp, i);
            if(i<levelNum/2)          pane.add(temp.get(i), i, 0);
            if(levelNum/2<=i)         pane.add(temp.get(i), i-4, 1);
        }

        //now pre-render progress
        root.getChildren().setAll(pane, preRenderProgress());
        setLabelsVisible(false);
    }

    private VBox preRenderProgress(){
        VBox progressPane = new VBox();
        personalBestLabel = new Label("Personal Best Records");
        personalBestLabel.setFont(new Font("Arial Black", 18));
        //number of discovered words
        wordsLabel = new Label("Words Discovered: ");
        wordsLabel.setFont(new Font("Arial Black", 15));
        wordsNum = new Label("0");
        wordsNum.setFont(new Font("Arial Black", 15));
        HBox wordsPane = new HBox(wordsLabel, wordsNum);
        wordsPane.setAlignment(Pos.CENTER_LEFT);
        wordsPane.setSpacing(5);
        //total score
        scoreLabel = new Label("Total Score: ");
        scoreLabel.setFont(new Font("Arial Black", 15));
        totalScore = new Label("0");
        totalScore.setFont(new Font("Arial Black", 15));
        HBox scorePane = new HBox(scoreLabel, totalScore);
        scorePane.setAlignment(Pos.CENTER_LEFT);
        scorePane.setSpacing(5);
        //add to progress pane
        progressPane.setAlignment(Pos.CENTER_LEFT);
        progressPane.setSpacing(5);
        progressPane.getChildren().addAll(personalBestLabel, wordsPane, scorePane);
        return progressPane;
    }

    private void setLabelsVisible(boolean visible){
        personalBestLabel.setVisible(visible);
        wordsLabel.setVisible(visible);
        wordsNum.setVisible(visible);
        scoreLabel.setVisible(visible);
        totalScore.setVisible(visible);
    }

    //set a letterbox on mouse interaction
    private void setLevelGridOnMouseInteraction(String gameMode, LevelBox list, int index){
        StackPane pane = list.get(index);
        Rectangle box = (Rectangle) pane.getChildren().get(0);
        Paint color = box.getStroke();
        pane.setOnMouseEntered(e -> box.setStroke(Color.DARKBLUE));
        pane.setOnMouseExited(e -> box.setStroke(color));
        pane.setOnMouseClicked(e -> handleMouseClickRequest(gameMode, list, index));
    }

    private void handleMouseClickRequest(String gameMode, LevelBox list, int index){
        levelSelected = index;
        StackPane pane = list.get(index);
        Rectangle box = (Rectangle) pane.getChildren().get(0);
        Paint color = box.getFill();
        for(StackPane p: list){
            Rectangle b = (Rectangle) p.getChildren().get(0);
            if(b.getFill().equals(Color.ORANGERED)){
                b.setFill(color);
            }
        }
        list.setBoxFillColor(index, Color.ORANGERED);
        showSelectedLevelProgress(gameMode, index);
    }

    //this method restore the level grid's background color if player clicks another game mode
    private void restoreLevelsBackgroundColor(Label oldGameMode, Label newGameMode, int levelSelected){
        LevelBox oldLevel = null;
        LevelBox newLevel = null;
        //find the old and new level
        for(int i=0; i<gameModesNames.length; i++) {
            if (oldGameMode.getTooltip().getText().equals(gameModesNames[i])) {
                oldLevel = allGameModes.get(i);
            }
            if (newGameMode.getTooltip().getText().equals(gameModesNames[i])){
                newLevel = allGameModes.get(i);
            }
        }
        //restore the old game mode's level color with the new game mode's level color
        assert newLevel != null;
        Color color = newLevel.defaultBackgroundColor;
        assert oldLevel != null;
        oldLevel.setBoxFillColor(levelSelected, color);
    }


    //show progress in selected level
    private void showSelectedLevelProgress(String gameMode, int level){
        //player has selected a level
        isLevelSelected = true;
        gameModeSelected = gameMode;
        levelSelected = level;
        //enable game start button
        workspace.getStartGameButton().setDisable(!isLevelSelected);

        //depending on which level and which game mode the player clicked, go fetch corresponding personal best records
        Map<Integer, GameData.GameProgressList> currentMode= null;
        for(int i=0; i<gameModesNames.length;i++){
            if(gameMode.equals(gameModesNames[i])){
                currentMode = allGameProgressMaps.get(i);
            }
        }
        assert currentMode != null;
        wordsNum.setText(String.valueOf(currentMode.get(level+1).wordsDiscovered));
        totalScore.setText(String.valueOf(currentMode.get(level+1).totalScore));
        setLabelsVisible(true);
    }

    //player's game progress has changed, need to update the level screen
    public void update(){
        int index = 0;
        for(int i=0; i<gameModesNames.length;i++){
            if(gameModeSelected.equals(gameModesNames[i])){
                index = i;
            }
        }
        //update data
        if(index == 0) allGameProgressMaps.set(index, gameData.getCommonProgressMap());
        if(index == 1) allGameProgressMaps.set(index, gameData.getPlacesProgressMap());
        if(index == 2) allGameProgressMaps.set(index, gameData.getCelebrityProgressMap());
        if(index == 3) allGameProgressMaps.set(index, gameData.getDictionaryWordsProgressMap());
        //render level screen
        allGameModes.get(index).markBox(levelSelected, true);
        if(levelSelected+1 < levelNumPerGameMode) {
            allGameModes.get(index).setBoxDisable(levelSelected + 1, false);
        }
    }

    //set when returning to profile page
    public void reset(){
        isLevelSelected = false;
        workspace.getStartGameButton().setDisable(true);
        setLabelsVisible(false);
        //restore level selected
        int index = 0;
        for(int i=0; i<gameModesNames.length;i++){
            if(gameModeSelected.equals(gameModesNames[i])){
                index = i;
            }
        }
        if(index == 0) allGameProgressMaps.set(index, gameData.getCommonProgressMap());
        if(index == 1) allGameProgressMaps.set(index, gameData.getPlacesProgressMap());
        if(index == 2) allGameProgressMaps.set(index, gameData.getCelebrityProgressMap());
        if(index == 3) allGameProgressMaps.set(index, gameData.getDictionaryWordsProgressMap());

        LevelBox current = allGameModes.get(index);
        Color color = current.defaultBackgroundColor;
        current.setBoxFillColor(levelSelected, color);

        //restore selections
        levelSelected = -1;
        gameModeSelected = "";
    }

    public static boolean getIsLevelSelected(){
        return isLevelSelected;
    }
    public static String getGameModeSelected(){
        return gameModeSelected;
    }
    public static int getLevelSelected(){
        return levelSelected;
    }
    public static void nextLevelSelected(){ levelSelected++;}
    public static int getCurrentBestDiscoveredWords(){
        return Integer.valueOf(wordsNum.getText());
    }
    public static int getCurrentBestTotalScore(){
        return Integer.valueOf(totalScore.getText());
    }
}
