package gui;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.effect.Light.Point;

/**
 * Graph component 2: Edge.
 * A graph contains vertices and edges.
 * Edges are the lines connecting vertices in a graph.
 * Note that in this app, all edges have the same weight, or no weight.
 *
 * @author Tianyi Lan
 */
public class Edge extends Line {

    private static final double DEFAULT_WIDTH = 3;
    private static final double DEFAULT_DASH_GAP = 4.4d;
    private static final Color HIGHLIGHT_STROKE_COLOR = Color.ORANGERED;
    private static final Color DEFAULT_STROKE_COLOR = Color.BLACK;

    private Vertex from;
    private Vertex to;

    public Edge(Vertex from, Vertex to, double width) {
        super();
        this.from = from;
        this.to = to;
        connect(from, to);
        setStrokeWidth(width);
        getStrokeDashArray().addAll(DEFAULT_DASH_GAP);
    }

    public Edge(Vertex from, Vertex to){
        this(from, to, DEFAULT_WIDTH);
    }

    public Vertex getFrom(){
        return from;
    }
    public Vertex getTo(){
        return to;
    }

    public void setHighlight(boolean state) {
        if (state) {
            this.setStroke(HIGHLIGHT_STROKE_COLOR);
            getStrokeDashArray().clear();

        } else {
            this.setStroke(DEFAULT_STROKE_COLOR);
            getStrokeDashArray().addAll(DEFAULT_DASH_GAP);
        }
    }

    private void connect(Vertex from, Vertex to){
        double fromX = from.getLayoutX();
        double fromY = from.getLayoutY();
        double toX   = to.getLayoutX();
        double toY   = to.getLayoutY();

        //from - to
        if(fromX < toX && fromY == toY){
            link(from.getRight(), to.getLeft());
        }

        //to - from
        if(fromX > toX && fromY == toY){
            link(from.getLeft(), to.getRight());
        }

        /*
          from
          |
          to
        */
        if(fromY < toY && fromX == toX){
            link(from.getBottom(), to.getTop());
        }

        /*
          to
          |
          from
        */
        if(fromY > toY && fromX == toX){
            link(from.getTop(), to.getBottom());
        }

        //finally, add this edge to both vertices' edge set
        from.addEdge(this);
        to.addEdge(this);
    }

    private void link(Point from, Point to){
        super.setStartX(from.getX());
        super.setStartY(from.getY());
        super.setEndX(to.getX());
        super.setEndY(to.getY());
    }
}
