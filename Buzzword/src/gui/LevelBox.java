package gui;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;

/**
 * Helper class that serves as the graphical shell of all levels
 *
 * @author Tianyi Lan
 */
//order of item adding onto StackPane: background -> letter(Text) -> mark
public class LevelBox extends ArrayList<StackPane> {
    public boolean allMarked;
    public double boxSize;
    public Color defaultBackgroundColor;
    //constructor
    public LevelBox(Text[] textArray, double boxSize) {
        this.boxSize = boxSize;
        this.allMarked = false;
        for (Text text : textArray) {
            Rectangle box = new Rectangle(boxSize, boxSize);
            box.setFill(Color.TRANSPARENT);
            box.setStroke(Color.BURLYWOOD);
            box.setStrokeWidth(2);
            this.add(new StackPane(box, text));
        }
    }

    //change fill color of text
    public void setTextFill(Color fillColor){
        for(StackPane pane: this) {
            Text text = (Text) pane.getChildren().get(1);
            text.setFill(fillColor);
        }
    }

    //change properties of box
    public void setBox(Color fillColor, Color outlineColor, double outLineWidth){
        defaultBackgroundColor = fillColor;
        for(StackPane pane: this) {
            Rectangle box = (Rectangle) pane.getChildren().get(0);
            box.setFill(fillColor);
            box.setStroke(outlineColor);
            box.setStrokeWidth(outLineWidth);
        }
    }

    //overloaded method, set background color of the box at the specific index
    public void setBoxFillColor(int index, Color fillColor){
        if(index >= 0 && index<this.size()){
            Rectangle box = (Rectangle) this.get(index).getChildren().get(0);
            box.setFill(fillColor);
        }
    }

    //method to mark/un-mark certain box, NOTE THAT for this app, it marks/un-marks certain level as cleared
    public void markBox(int index, boolean isCleared){
        if(!allMarked){
            for(StackPane pane: this) {
                Text clear = new Text("Cleared");
                clear.setRotate(340);
                clear.setFont(new Font("Arial Black", boxSize * 0.23));
                clear.setVisible(false);
                pane.getChildren().add(clear);
            }
            allMarked = true;
        }
        //mark/un-mark certain box
        Text temp = (Text) this.get(index).getChildren().get(2);
        temp.setVisible(isCleared);
    }

    //method to set certain box enable/disable
    public void setBoxDisable(int index, boolean disable){
        StackPane pane = this.get(index);
        //Rectangle box = (Rectangle) pane.getChildren().get(0);
        Paint color = defaultBackgroundColor;
        //disable the box
        if(disable) {
            pane.setDisable(true);
            setBoxFillColor(index, Color.DARKGRAY);
        }
        //enable the box
        else{
            pane.setDisable(false);
            setBoxFillColor(index, (Color) color);
        }
    }
}
