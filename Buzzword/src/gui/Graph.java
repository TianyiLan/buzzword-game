package gui;

import javafx.scene.layout.Pane;

import java.beans.VetoableChangeListener;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A graph contains vertices and edges.
 * Note that the graph created in the app is a simple undirected graph.
 * Also note that the graph is a 4 x 4 grid,
 * and each vertex is connected to its top, bottom, left and right neighbours.
 *
 * @author Tianyi Lan
 */
public class Graph extends Pane {
    private static final double VERTEX_GAP = 75;
    private static final int NUMBER_OF_SIZE = 4;
    private static final String[] LETTER_BANK
            = {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P",
            "A", "S", "D", "F", "G", "H", "J", "K", "L",
            "Z", "X", "C", "V", "B", "N", "M"};
    private static final String[] VOWEL_BANK ={"A", "O", "I", "E","U"};

    private ArrayList<ArrayList<Vertex>> paths;
    private ArrayList<Vertex> vertices;
    private ArrayList<Edge> edges;
    private char[][] matrix;

    public Graph(){
        super();
        layoutGrid();
        generate();
    }

    private void layoutGrid() {
        paths = new ArrayList<>();
        vertices = new ArrayList<>();
        edges = new ArrayList<>();
        int id = 0;
        Vertex from;
        Vertex to;

        //creates all vertices
        for (int i = 0; i < NUMBER_OF_SIZE; i++) {
            for (int j = 0; j < NUMBER_OF_SIZE; j++) {
                Vertex vertex = new Vertex(10 + j * VERTEX_GAP, 10 + i * VERTEX_GAP, id);
                vertices.add(vertex);
                id++;
            }
        }

        //links all vertices
        for (int i = 0; i < NUMBER_OF_SIZE; i++) {
            for (int j = 0; j < NUMBER_OF_SIZE; j++) {
                //horizontal
                if (j < NUMBER_OF_SIZE - 1) {
                    from = vertices.get(i * NUMBER_OF_SIZE + j);
                    to = vertices.get(i * NUMBER_OF_SIZE + j + 1);
                    Edge hEdge = new Edge(from, to);
                    edges.add(hEdge);
                }
                //vertical
                if (i < NUMBER_OF_SIZE - 1) {
                    from = vertices.get(i * NUMBER_OF_SIZE + j);
                    to = vertices.get((i + 1) * NUMBER_OF_SIZE + j);
                    Edge vEdge = new Edge(from, to);
                    edges.add(vEdge);
                }
            }
        }
        //sets neighbours for all vertices, add them to pane
        for (Vertex vertex : vertices) {
            vertex.setNeighbours();
            this.getChildren().add(vertex);
        }
        //add all edges to pane
        for (Edge edge : edges) {
            this.getChildren().add(edge);
        }
    }

    //random vertex
    private Vertex randomVertex(ArrayList<Vertex> list) {
        ArrayList<Vertex> unmarked = list.stream().
                filter(v -> !v.isMarked()).collect(Collectors.toCollection(ArrayList::new));
        int random = (int) (Math.random() * (unmarked.size()));
        return unmarked.get(random);
    }

    private String randomLetter(String[] bank){
        int random = (int) (Math.random() * bank.length);
        return bank[random];
    }

    public void generate(){
        Vertex current;
        for(int i=0; i<vertices.size(); i++){
            //generate 2 vowels first
            if(i <= 1){
                current = randomVertex(vertices);
                current.setLetter(randomLetter(VOWEL_BANK));
                current.setMarked(true);
            }
            //then fill the rest with random letters
            else{
                current = randomVertex(vertices);
                current.setLetter(randomLetter(LETTER_BANK));
                current.setMarked(true);
            }
        }
        unMark();
    }

    private char getCharFromID(int id){
        if(id>=0 && id<=15){
            return vertices.get(id).getChar();
        }
        else{
            return 0;
        }
    }

    /** A method to solve the grid and return a map consist of all solutions*/
    public Map<String, Integer> solve(Map<String, Integer> wordsMap){
        Map<String, Integer> minSolution = new HashMap<>();
        //create a matrix in char[][] for searching
        matrix = new char[NUMBER_OF_SIZE][NUMBER_OF_SIZE];
        int index = 0;
        for(int row = 0; row< NUMBER_OF_SIZE; row++){
            for(int col = 0; col< NUMBER_OF_SIZE; col++){
                matrix[row][col] = getCharFromID(index);
                index++;
            }
        }
        //searching
        wordsMap.keySet().stream().filter(test ->
                isWordValid(matrix, test)).forEach(test -> minSolution.put(test, wordsMap.get(test)));
        return minSolution;
    }

    /** A method to test if a word is valid, return TRUE if the word can be formed in the grid */
    private boolean isWordValid(char[][] matrix, String word) {
        for (int row = 0; row < NUMBER_OF_SIZE; row++) {
            for (int col = 0; col < NUMBER_OF_SIZE; col++) {
                if (backtrackingSearch(matrix, word, row, col)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * A method to backtracking-search recursively.
     * Note that the searching is done letter by letter from the leading letter to the end,
     * so define 2 base cases to end the search, other than these 3 cases, keeping searching:
     * 1. When search done, the given word should be an empty string, meaning search succeeded
     * 2. For this app, we only search in 4 directions, so when out of boundary, search failed
     * 3. If current letter is not in the cell, search failed
     *
     * */
    private boolean backtrackingSearch(char[][] matrix, String word, int row, int col) {
        //base case 1: when searching done, it should be an empty string
        if (word.equals("")) {
            return true;
        }
        //base case 2: search in 4 directions, if out of the boundary of the grid, then false
        else if (row < 0 || row >= NUMBER_OF_SIZE || col < 0 || col >= NUMBER_OF_SIZE ){
            return false;
        }
        else{
            //base case 3: current letter not in the cell
            if (matrix[row][col] != word.charAt(0)){
                return false;
            }
            //other than above 3 cases, keeping searching by recursive calls
            else {
                char temp = matrix[row][col];
                //mark this cell as visited
                matrix[row][col] = '$';
                //move to next char
                //String rest = word.substring(1);
                String rest = word.substring(1, word.length());
                //test top, bottom, left and right neighbours
                boolean isValid = backtrackingSearch(matrix, rest, row-1, col) ||  //top
                        backtrackingSearch(matrix, rest, row+1, col) ||            //bottom
                        backtrackingSearch(matrix, rest, row, col-1) ||            //left
                        backtrackingSearch(matrix, rest, row, col+1);              //right
                //un-mark the cell for the next search
                matrix[row][col] = temp;
                return isValid;
            }
        }
    }

    /**
     * Finds all vertices that contain the specified letter
     *
     * @param target The specified letter
     * @return All vertices that contain the target letter
     */
    public List<Vertex> find(char target){
        return vertices.stream().filter(v -> v.getChar() == target).collect(Collectors.toList());
    }

    public List<Vertex> find(String target){
        return find(target.toLowerCase().charAt(0));
    }

    public void clear(){
        for(Vertex v : vertices) {
            v.setMarked(false);
            v.setHighlight(false);
            v.setPathHead(false);
        }
        for(Edge e: edges){
            e.setHighlight(false);
        }
    }
    public void clearPath(){
        paths.clear();
    }

    private void unhighlight(){
        for(Vertex v : vertices) {
            v.setHighlight(false);
        }
        for(Edge e: edges){
            e.setHighlight(false);
        }
    }

    public void reGenerate(){
        clear();
        generate();
    }


    public void unMark(){
        for (Vertex v : vertices) {
            v.setMarked(false);
        }
    }

    public void highlightAllPaths(){
        if(paths.isEmpty()){
            return;
        }
        else{
            unhighlight();
            for(ArrayList<Vertex> path: paths){
                highlightAPath(path);
            }
        }
    }

    private void highlightAPath(ArrayList<Vertex> path){
        if(path.isEmpty()){
            return;
        }
        if(path.size() == 1){
            path.get(0).setHighlight(true);
        }
        else{
            for(int i=0; i<path.size(); i++){
                path.get(i).setHighlight(true);
                if(i<path.size()-1){
                    highlightLinkedEdge(path.get(i), path.get(i+1));
                }
            }
        }
    }

    private void highlightLinkedEdge(Vertex from, Vertex to){
        Edge e = from.getCommonEdge(to);
        e.setHighlight(true);
    }



    public void findPaths(String letter){
        //first letter
        if(paths.isEmpty()) {
            for (Vertex v : find(letter)) {
                ArrayList<Vertex> path = new ArrayList<>();
                path.add(v);
                paths.add(path);
            }
        }
        //rest letters
        else{
            ArrayList<ArrayList<Vertex>> newPaths = new ArrayList<>();
            for(ArrayList<Vertex> path: paths){
                Vertex head = path.get(path.size()-1);
                List<Vertex> adj = head.getAvailableNeighbourWith(path, letter);
                if(!adj.isEmpty()){
                    if(adj.size() == 1){
                        path.add(adj.get(0));
                    }
                    else {
                        for (Vertex neighbour : adj) {
                            ArrayList<Vertex> childPath = new ArrayList<>();
                            childPath.addAll(path);
                            childPath.add(neighbour);
                            newPaths.add(childPath);
                        }
                    }
                }
            }
            paths.addAll(newPaths);
        }
    }

    public void removeInvalidPaths(String guess){
        ArrayList<ArrayList<Vertex>> temp = new ArrayList<>();
        for(int i=0; i<paths.size(); i++){
            if(paths.get(i).size() == guess.length()){
                temp.add(paths.get(i));
            }
        }
        paths = temp;
    }

    public boolean contains(String letter){
        for(Vertex v: vertices){
            if(v.contains(letter)){
                return true;
            }
        }
        return false;
    }

    public boolean hasNoPath(){return paths.isEmpty();}
    public List<Vertex> getVertices(){
        return vertices;
    }
    public List<Edge> getEdges(){
        return edges;
    }
}
