package gui;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.Window;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static buzzword.BuzzwordProperties.*;

/**
 * This class is the confirmation dialog model for user to confirm the action.
 *
 * --- Important Note ---
 * JavaFX dialogs can only be closed 'abnormally' (as defined above) in two situations:
 * 1. When the dialog only has one button, or
 * 2. When the dialog has multiple buttons, as long as one of them meets one of the following requirements:
 *   (1) The button has a ButtonType whose ButtonBar.ButtonData is of type ButtonBar.ButtonData.CANCEL_CLOSE.
 *   (2) The button has a ButtonType whose ButtonBar.ButtonData returns true when ButtonBar.ButtonData.isCancelButton() is called.
 *
 * @author Tianyi Lan
 */
public class YesNoDialog {
    private Alert dialog;
    private boolean isUserSure;

    public YesNoDialog() {
        dialog = new Alert(Alert.AlertType.CONFIRMATION);
        isUserSure = false;
    }

    public void close(){
        dialog.hide();
    }
    public boolean isShowing(){return dialog.isShowing();}

    public boolean show(String actionToConfirm) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        //set icon for this dialog
        Image icon = (AppGUI.initializeImageView(BUZZWORD_ICON.toString()).getImage());
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        //set image
        dialog.setGraphic(AppGUI.initializeImageView(QUESTION_IMAGE.toString()));
        //set title and message
        dialog.setTitle(propertyManager.getPropertyValue(CONFIRMATION_TITLE));
        dialog.setHeaderText(null);
        dialog.setContentText(propertyManager.getPropertyValue(actionToConfirm));


        ButtonType yes = new ButtonType(null);
        ButtonType no = new ButtonType(null);
        dialog.getButtonTypes().setAll(yes, no);
        Button yesButton = (Button) dialog.getDialogPane().lookupButton(yes);
        Button noButton = (Button) dialog.getDialogPane().lookupButton(no);
        Workspace.decButton(yesButton, OK_IMAGE.toString(), OK_TOOLTIPS.toString());
        Workspace.decButton(noButton, CANCEL_IMAGE.toString(), CANCEL_TOOLTIPS.toString());
        setCloseButtonX();

        dialog.showAndWait().ifPresent(response -> {
            if(response == no) {
                dialog.close();
            }
            if(response == yes){
                isUserSure = true;
            }
        });
        return isUserSure;
    }

    //close the dialog when clicking the close button X
    private void  setCloseButtonX(){
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> window.hide());
    }
}
