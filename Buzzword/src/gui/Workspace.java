package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import data.GameData;
import data.GameTimer;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static buzzword.BuzzwordProperties.*;
import static settings.AppPropertyType.EXIT_ICON;
import static settings.AppPropertyType.EXIT_TOOLTIP;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * This class serves as the GUI component for the Buzzword game.
 *
 * @author Tianyi Lan
 */
public class Workspace extends AppWorkspaceComponent {

    private ImageView                guiHeading;        // workspace (GUI) heading
    private HBox                     headPane;          // container to display the heading
    private HBox                     bodyPane;          // container for the main game displays
    private VBox                     gameTextsPane;     // container to display the text-related parts of the game
    private HBox                     timerBox;          // text area displaying the game timer
    private HBox                     targetScoreBox;    // container for target score display
    private HBox                     totalScoreBox;     // container for total score display
    private HBox                     guessBox;          // container for guessed word display
    private VBox                     triedLetters;      // container to display the letter grid
    private VBox                     tableBox;          // container for game progress table display
    private Graph                    grid;              // shared reference to the graph
    private Label                    guess;             // label to update current guess
    private Label                    targetScore;       // label to update target score
    private Label                    totalScore;        // label to update total score
    private Label                    timer;             // shared reference to the game timer

    private Button             startGame;         // the button to start playing a game of Buzzword game
    private Button             createProfile;     // the button to create a new profile
    private Button             login;             // the button to login
    private Button             exit;              // the button to exit the application
    private Button             resume;            // the button to resume the game
    private Button             pause;             // the button to pause the game
    private Button             replay;            // the button to replay the current level
    private Button             toProfilePage;     // the button to return to profile page
    private Button             help;              // the button to show help information
    private Button             logout;            // the button to logout
    private Button             delete;            // the button to delete the current profile
    private Button             next;              // the button to play the next level
    private Button             change;            // the button to change user's password

    private AppTemplate        app;               // the actual application
    private AppGUI             gui;               // the GUI inside which the application sits
    private BuzzwordController controller;        // shared reference to the game controller
    private GameData           gameData;          // shared reference to the game data

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (BuzzwordController) gui.getFileController();
    }


    /**
     * Layout play screen.
     * Note that parameter "level" starts at 0
     *
     * @param gameMode game mode to play
     * @param level level to play
     */
    public void layoutPlayScreen(String gameMode, int level){
        //sync game data
        gameData = (GameData) app.getDataComponent();


        bodyPane = new HBox();
        bodyPane.setAlignment(Pos.TOP_CENTER);
        bodyPane.setSpacing(200);

        //right
        gameTextsPane = new VBox();
        timerBox = new HBox();
        targetScoreBox = new HBox();
        totalScoreBox = new HBox();
        guessBox = new HBox();

        //progress table
        layoutTable();

        //timer
        timer = GameTimer.formatTime(controller.calTime(level+1));
        timer.setFont(new Font("Arial Black", 13));
        Label remainingTime = new Label("Remaining Time: ");
        remainingTime.setFont(new Font("Arial Black", 13));
        timerBox.getChildren().addAll(remainingTime, timer);

        //target score
        targetScore = new Label("0");
        targetScore.setFont(new Font("Arial Black", 16));
        Label scoreLabel = new Label("Target Score: ");
        scoreLabel.setFont(new Font("Arial Black", 16));
        targetScoreBox.getChildren().addAll(scoreLabel, targetScore);

        //total score
        totalScore = new Label("0");
        totalScore.setFont(new Font("Arial Black", 16));
        Label totalScoreLabel = new Label("Total Score: ");
        totalScoreLabel.setFont(new Font("Arial Black", 16));
        totalScoreBox.getChildren().addAll(totalScoreLabel, totalScore);

        //current guess
        guess = new Label();
        guess.setFont(new Font("Arial Black", 13));
        Label guessLabel = new Label("Guess: ");
        guessLabel.setFont(new Font("Arial Black", 13));
        guessBox.getChildren().addAll(guessLabel, guess);

        //grid
        Label mode = new Label("Game Mode: " + gameMode);
        mode.setFont(new Font("Arial Black", 16));
        Label current = new Label("Current Level: " + (level + 1));
        current.setFont(new Font("Arial Black", 16));
        triedLetters = new VBox();
        triedLetters.setAlignment(Pos.CENTER_LEFT);
        triedLetters.setSpacing(10);
        triedLetters.getChildren().addAll(mode, current, generateLetterGrid(level));

        //now layout all play screen components
        gui.getAppPane().setCenter(bodyPane);
        bodyPane.getChildren().clear();
        bodyPane.getChildren().addAll(triedLetters, gameTextsPane);
        gameTextsPane.getChildren().clear();
        gameTextsPane.getChildren().addAll(
                timerBox,
                guessBox,
                tableBox,
                totalScoreBox,
                targetScoreBox);
    }

    public void hideLetterGird(boolean state){
        grid.setVisible(!state);
    }

    public void layoutGUI() throws IOException {
        guiHeading = AppGUI.initializeImageView(WORKSPACE_HEADING.toString());

        headPane = new HBox();
        headPane.getChildren().add(guiHeading);
        headPane.setAlignment(Pos.CENTER);

        //build all buttons
        startGame = AppGUI.initializeChildButton(PLAY_IMAGE.toString(), PLAY_TOOLTIPS.toString(), false);
        createProfile = AppGUI.initializeChildButton(PROFILE_IMAGE.toString(), PROFILE_TOOLTIPS.toString(), false);
        login = AppGUI.initializeChildButton(LOGIN_IMAGE.toString(), LOGIN_TOOLTIPS.toString(), false);
        exit = AppGUI.initializeChildButton(EXIT_ICON.toString(), EXIT_TOOLTIP.toString(), false);
        resume = AppGUI.initializeChildButton(RESUME_IMAGE.toString(), RESUME_TOOLTIPS.toString(), false);
        pause = AppGUI.initializeChildButton(PAUSE_IMAGE.toString(), PAUSE_TOOLTIPS.toString(), false);
        replay = AppGUI.initializeChildButton(REPLAY_IMAGE.toString(), REPLAY_TOOLTIPS.toString(), false);
        toProfilePage = AppGUI.initializeChildButton(MENU_IMAGE.toString(), MENU_TOOLTIPS.toString(), false);
        help = AppGUI.initializeChildButton(HELP_IMAGE.toString(), HELP_TOOLTIPS.toString(), false);
        logout = AppGUI.initializeChildButton(LOGOUT_IMAGE.toString(), LOGOUT_TOOLTIPS.toString(), false);
        delete = AppGUI.initializeChildButton(DELETE_IMAGE.toString(), DELETE_TOOLTIPS.toString(), false);
        next = AppGUI.initializeChildButton(NEXT_IMAGE.toString(), NEXT_TOOLTIPS.toString(), false);
        change = AppGUI.initializeChildButton(CHANGE_IMAGE.toString(),CHANGE_TOOLTIPS.toString(), false);

        updateToolbar(controller.getGameState());
        gui.getAppPane().setCenter(null);
        gui.getAppPane().setTop(headPane);

        setupHandlers(); // ... and set up event handling
    }

    private void setupHandlers() {
        pause.setOnAction(e -> controller.handlePauseRequest());

        resume.setOnAction(e -> controller.handleResumeRequest());

        startGame.setOnAction(e -> controller.handleStartGameRequest());

        exit.setOnAction(e -> {
            try {
                controller.handleExitRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        next.setOnAction(e -> {
            controller.handleNextRequest();
        });

        createProfile.setOnAction(e -> {
            try {
                controller.handleProfileCreationRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        change.setOnAction(e ->{
            try {
                controller.handleChangePWRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        login.setOnAction(e -> {
            try {
                controller.handleLoginRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        logout.setOnAction(e -> {
            try {
                controller.handleLogoutRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        toProfilePage.setOnAction(e -> {
            try {
                controller.handlerToProfilePageRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        replay.setOnAction(e -> {
            try {
                controller.handleReplayRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        help.setOnAction(e -> {
            try {
                controller.handleHelpRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        delete.setOnAction(e -> {
            try {
                controller.handleDeleteRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }

    /**
     * This function displays and enables/disables certain buttons depending on different game state.
     */
    public void updateToolbar(BuzzwordController.GameState gameState) {
        HBox pane = null;
        HBox right = null;
        if(controller.gamedata != null) {
            Label currentUser = new Label("Current Player: " + controller.gamedata.getUsername());
            Label temp =  new Label("Current Player: " + controller.gamedata.getUsername());
            currentUser.setFont(Font.font("Cambria", 15));
            temp.setFont(Font.font("Cambria", 15));
            temp.setTextFill(Color.TRANSPARENT);
            pane = new HBox(currentUser);
            right = new HBox(temp);
            right.setAlignment(Pos.CENTER_RIGHT);
            pane.setAlignment(Pos.CENTER_LEFT);
        }

        if(gameState == BuzzwordController.GameState.NOT_LOGIN){
            gui.getToolbarPane().getChildren().setAll(createProfile, login, help, exit);
        }
        if(gameState == BuzzwordController.GameState.LOGIN_NOT_STARTED){
            gui.getToolbarPane().getChildren().setAll(pane,startGame, logout, change, delete, help, exit, right);
            HBox.setHgrow(pane, Priority.ALWAYS);
            HBox.setHgrow(right, Priority.ALWAYS);
            gui.getToolbarPane().setAlignment(Pos.CENTER);
            startGame.setDisable(true);
        }
        if(gameState == BuzzwordController.GameState.LOGIN_STARTED){
            gui.getToolbarPane().getChildren().setAll(pane, pause, replay, toProfilePage, logout, help, exit, right);
            HBox.setHgrow(pane, Priority.ALWAYS);
            HBox.setHgrow(right, Priority.ALWAYS);
            gui.getToolbarPane().setAlignment(Pos.CENTER);
        }
        if(gameState == BuzzwordController.GameState.ENDED){
            gui.getToolbarPane().getChildren().setAll(pane, next, replay, toProfilePage, logout, help, exit, right);
            HBox.setHgrow(pane, Priority.ALWAYS);
            HBox.setHgrow(right, Priority.ALWAYS);
            gui.getToolbarPane().setAlignment(Pos.CENTER);
        }
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(BOTTOM_TOOLBAR));
    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; this program wont't be using this method */
    }

    /** This function reinitialize the GUI */
    public void reInitGUI(){
        updateToolbar(controller.getGameState());
        gui.getAppPane().setCenter(null);
        gui.getAppPane().setTop(headPane);
    }

    //public helper method to decorate an instantiated button
    public static void decButton(Button button, String icon, String tooltip) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            button.setGraphic(new ImageView(buttonImage));
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            button.setTooltip(buttonTooltip);
            button.setFocusTraversable(false);
            button.setStyle("-fx-border-color: transparent");
            button.setOnMouseEntered(e -> button.setStyle("-fx-border-color: orange"));
            button.setOnMouseExited(e -> button.setStyle("-fx-border-color: transparent"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public Button getResumeButton(){return resume;}
    public Button getPauseButton(){return pause;}
    public Button getStartGameButton(){return startGame;}
    public Button getNextButton(){return next;}
    public Label getTimerLabel(){return timer;}
    public Label getGuessLabel(){return guess;}
    public Label getTotalScoreLabel(){return totalScore;}
    public Label getTargetScoreLabel(){return targetScore;}
    public Graph getGraph(){return grid;}


    private Graph generateLetterGrid(int currentLevel){
        PropertyManager pm = PropertyManager.getManager();
        int tarScore = Integer.valueOf(pm.getPropertyValue(BASE_TARGET_SCORE)) + ((currentLevel) / 2) * 20 ;
        targetScore.setText(String.valueOf(tarScore));
        grid = new Graph();
        return grid;
    }


    @SuppressWarnings("unchecked")
    private void layoutTable(){
        tableBox = new VBox();
        tableBox.setSpacing(5);
        tableBox.setPadding(new Insets(10, 0, 0, 0));

        Label label = new Label("Game Progress");
        label.setFont(new Font("Arial Black", 20));

        TableView<BuzzwordController.Entry> table = new TableView<>();
        table.setPlaceholder(new Label("No word discovered"));
        table.setEditable(false);
        table.setFocusTraversable(false);
        table.setPrefSize(230, 260);
        table.setItems(controller.getTableData());
        TableColumn<BuzzwordController.Entry, String> wordCol = new TableColumn<>("Discovered");
        wordCol.setResizable(false);
        wordCol.setSortable(false);
        wordCol.setPrefWidth(160);
        TableColumn<BuzzwordController.Entry, String> scoreCol = new TableColumn<>("Score");
        scoreCol.setResizable(false);
        scoreCol.setSortable(false);
        scoreCol.setPrefWidth(68);
        table.getColumns().addAll(wordCol, scoreCol);
        wordCol.setCellValueFactory(cellData -> cellData.getValue().getWordProp());
        scoreCol.setCellValueFactory(cellData -> cellData.getValue().getScoreProp());

        tableBox.getChildren().addAll(label, table);
    }
}


