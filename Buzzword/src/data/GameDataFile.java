package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;
import propertymanager.PropertyManager;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static buzzword.BuzzwordProperties.*;
import static settings.AppPropertyType.APP_TITLE;
import static settings.AppPropertyType.WORK_FILE_EXT;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 *
 * Note that for this game, the file system is divided into two parts.
 * The first part contains only one file, which name should be Username.json,
 * storing all players' username and password, used for quick searching.
 * The second part contains a list of files, each of which stores a specific
 * player's profile with its username as the file name.
 *
 * @author Ritwik Banerjee
 * @author Tinayi Lan
 */
public class GameDataFile implements AppFileComponent {

    private static final String DOT = ".";
    private static final String UNDERLINE = "_";

    /**
     * When creating a new profile, update the user info file and create a corresponding game progress file.
     *
     * @param data The game data
     * @param username The username of the profile about to create
     */
    public void createProfileFile(GameData data, String username){
        writeUserInfoFile(data);
        saveData(data, getFilePath(username));
    }

    /**
     * After player has changed the password, update the user info file.
     * @param data The game data
     */
    public void updateUserInfo(GameData data){
        writeUserInfoFile(data);
    }

    /**
     * When deleting a profile, update the user info file and delete the corresponding game progress file.
     *
     * @param data The game data
     * @param username The username of the profile about to delete
     */
    public void deleteProfileFile(GameData data, String username) throws IOException {
        //update user info file
        writeUserInfoFile(data);
        //delete the corresponding game progress file
        if(Files.exists(getFilePath(username))) {
            Files.delete(getFilePath(username));
        }
    }

    /**
     * When logging in a profile, load its game progress
     *
     * @param data The game data
     * @param username The username of the profile which game progress about to load
     */
    public void loadGameProgress(GameData data, String username) throws IOException {
        loadData(data, getFilePath(username));
    }

    public void saveGameProgress(GameData data, String username){
        saveData(data, getFilePath(username));
    }

    /**
     * When starting the game, load all user info
     *
     * @param data The game data
     */
    public void loadUserInfo(GameData data) throws IOException {
        readUserInfoFile(data);
    }

    /**
     * Test if user info file exists
     *
     * @return whether or not the file exists
     */
    public boolean userInfoFileExists(){
        PropertyManager propertyManager = PropertyManager.getManager();
        String fileName = propertyManager.getPropertyValue(USER_INFO_FILE_NAME);
        return Files.exists(getFilePath(fileName));
    }

    @Override
    public void saveData(AppDataComponent data, Path to) {
        PropertyManager propertyManager = PropertyManager.getManager();
        GameData gamedata = (GameData) data;
        Map<Integer, GameData.GameProgressList> animalProgressMap = gamedata.getCommonProgressMap();
        Map<Integer, GameData.GameProgressList> placesProgressMap = gamedata.getPlacesProgressMap();
        Map<Integer, GameData.GameProgressList> celebrityProgressMap = gamedata.getCelebrityProgressMap();
        Map<Integer, GameData.GameProgressList> dictionaryWordsProgressMap =gamedata.getDictionaryWordsProgressMap();

        String gameMode;

        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(to)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();

            //write animal fields
            gameMode = propertyManager.getPropertyValue(GAMEMODE_COMMON_TOOLTIPS);
            writeGameModeFields(generator, gameMode, animalProgressMap);
            //write places fields
            gameMode = propertyManager.getPropertyValue(GAMEMODE_PLACES_TOOLTIPS);
            writeGameModeFields(generator, gameMode, placesProgressMap);
            //write celebrity fields
            gameMode = propertyManager.getPropertyValue(GAMEMODE_NAME_TOOLTIPS);
            writeGameModeFields(generator, gameMode, celebrityProgressMap);
            //write dictionary words fields
            gameMode = propertyManager.getPropertyValue(GAMEMODE_DICTIONARY_TOOLTIPS);
            writeGameModeFields(generator, gameMode, dictionaryWordsProgressMap);

            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        GameData gamedata = (GameData) data;
        gamedata.resetGameModesMaps();

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(from));
        int levelTotalNum = Integer.valueOf(propertyManager.getPropertyValue(NUMBER_LEVELS_IN_EACH_MODE));
        int level = 1;
        String animal = propertyManager.getPropertyValue(GAMEMODE_COMMON_TOOLTIPS);
        String places = propertyManager.getPropertyValue(GAMEMODE_PLACES_TOOLTIPS);
        String celebrity = propertyManager.getPropertyValue(GAMEMODE_NAME_TOOLTIPS);
        String dictionary = propertyManager.getPropertyValue(GAMEMODE_DICTIONARY_TOOLTIPS);

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                //read animal fields
                if(fieldname.contains(animal)){
                    readGameModeFields(jsonParser, animal, level, gamedata);
                    level++;
                    //reset level if one mode has finished reading
                    if(fieldname.equals(animal + UNDERLINE + levelTotalNum)){
                        level = 1;
                    }
                }
                //read places fields
                if(fieldname.contains(places)){
                    readGameModeFields(jsonParser, places, level, gamedata);
                    level++;
                    //reset level if one mode has finished reading
                    if(fieldname.equals(places + UNDERLINE + levelTotalNum)){
                        level = 1;
                    }
                }
                //read celebrity fields
                if(fieldname.contains(celebrity)){
                    readGameModeFields(jsonParser, celebrity, level, gamedata);
                    level++;
                    //reset level if one mode has finished reading
                    if(fieldname.equals(celebrity + UNDERLINE + levelTotalNum)){
                        level = 1;
                    }
                }
                //read dictionary words fields
                if(fieldname.contains(dictionary)){
                    readGameModeFields(jsonParser, dictionary, level, gamedata);
                    level++;
                    //reset level if one mode has finished reading
                    if(fieldname.equals(dictionary + UNDERLINE + levelTotalNum)){
                        level = 1;
                    }
                }
            }
        }
    }

    /**
     * Helper method to find the path of work file
     *
     * @return The path name of the file.
     * */
    private Path getFilePath(String fileName){
        PropertyManager propertyManager = PropertyManager.getManager();
        //find designated work directory
        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
        //specify the file name
        String extendedFileName = fileName + DOT + propertyManager.getPropertyValue(WORK_FILE_EXT);
        File file = new File(targetPath.toString(), extendedFileName);
        return file.toPath();
    }

    /**
     * Helper method to write to game progress JSON file
     *
     * @param generator The JSON generator
     * @param gameMode The game mode string
     * @param gameProgressMap The corresponding game progress map
     * */
    private void writeGameModeFields(JsonGenerator generator, String gameMode, Map<Integer, GameData.GameProgressList> gameProgressMap) throws IOException {
        //write each level
        for(Map.Entry<Integer, GameData.GameProgressList> entry: gameProgressMap.entrySet()){
            generator.writeFieldName(gameMode + UNDERLINE + entry.getKey());
            generator.writeStartArray(entry.getValue().size());
            //3 game progress fields
            generator.writeBoolean(entry.getValue().cleared);
            generator.writeNumber(entry.getValue().wordsDiscovered);
            generator.writeNumber(entry.getValue().totalScore);
            //end array
            generator.writeEndArray();
        }
    }

    /**
     * Helper method to read from game progress JSON file
     *
     * @param jsonParser The JSON parser
     * @param gameMode The game mode string
     * @param level The game level in certain game mode
     * @param gamedata The game data
     * */
    private void readGameModeFields(JsonParser jsonParser, String gameMode, int level, GameData gamedata) throws IOException {
        jsonParser.nextToken();
        int i = 1;
        boolean isCleared = false;
        int wordsDiscovered = 0;
        int totalScore = 0;
        while (jsonParser.nextToken() != JsonToken.END_ARRAY){
            if(i == 1){
                isCleared = jsonParser.getValueAsBoolean();
            }
            if(i == 2){
                wordsDiscovered = jsonParser.getValueAsInt();
            }
            if(i == 3){
                totalScore = jsonParser.getValueAsInt();
            }
            i++;
        }
        gamedata.updateGameProgress(gameMode, level, isCleared, wordsDiscovered, totalScore);
    }

    /**
     * Create a JSON file if non, or write to the file based on the information from the game data.
     * Note that this method can either create or delete a profile, depending on the game data.
     *
     * @param data The game data.
     * */
    private void writeUserInfoFile(GameData data){
        PropertyManager propertyManager = PropertyManager.getManager();
        Map<String, String> userinfoMap = data.getUserInfoMap();
        JsonFactory jsonFactory = new JsonFactory();
        //file name of user info file
        String fileName = propertyManager.getPropertyValue(USER_INFO_FILE_NAME);
        try (OutputStream out = Files.newOutputStream(getFilePath(fileName))) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();

            //write to file
            for(Map.Entry<String, String> entry: userinfoMap.entrySet()){
                generator.writeStringField(entry.getKey(), entry.getValue());
            }

            generator.writeEndObject();
            generator.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Read from the user info JSON file.
     *
     * @param data The game data.
     * */
    private void readUserInfoFile(GameData data) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        //reset user info map
        data.resetUserInfoMap();
        //file name of user info file
        String fileName = propertyManager.getPropertyValue(USER_INFO_FILE_NAME);

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(getFilePath(fileName)));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                jsonParser.nextToken();
                data.updateUserInfoMap(fieldname, jsonParser.getText());
            }
        }
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}