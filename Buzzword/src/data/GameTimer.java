package data;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * @author Tianyi Lan
 */

//an in-game timer that tells player how much time has elapsed
public class GameTimer {
    private Timeline timer;
    private int hour;
    private int min;
    private int sec;
    private int timeInSec;
    private String timeInString;

    //constructor
    public GameTimer(){
        hour = 0;
        min = 0;
        sec = 0;
        timeInSec = 0;
        timeInString = "";
        timer = new Timeline();
    }

    public String getTimeInFormat(){return timeInString;}
    public int getTimeInSec(){return timeInSec;}

    //this method is used for recording ELAPSED TIME
    public void startElapsingTimer(Label label){
        timer.setCycleCount(Animation.INDEFINITE);
        //update timer every seconds
        timer.getKeyFrames().add(new KeyFrame(Duration.seconds(1), e -> {
            sec++;
            timeInSec++;
            if(sec == 60){
                sec = 0;
                min++;
            }
            if(min == 60){
                min = 0;
                hour++;
            }
            if(hour == 99){
                hour = 0;
            }
            //format time
            formatTime(label);
        }));
        timer.playFromStart();
    }

    //this method is user for recording COUNTDOWN TIME
    public void startCountdownTimer(int countdownTimeInSec, Label label){
        //first set timeInSec
        timeInSec = countdownTimeInSec;
        //then convert timeInSec to hour/min/sec
        convertSecToHourMinSec(timeInSec);

        timer.setCycleCount(Animation.INDEFINITE);
        //update timer every seconds
        timer.getKeyFrames().add(new KeyFrame(Duration.seconds(1), e -> {
            sec--;
            timeInSec--;
            if (sec < 0) {
                sec = 59;
                min--;
            }
            if (min < 0) {
                if(hour <= 0){
                    min = 0;
                }
                else {
                    min = 59;
                }
                hour--;
            }
            if(hour <= 0){
                hour = 0;
            }
            //format time
            formatTime(label);
            if(timeInSec<30){
                label.setTextFill(Color.RED);
            }
            //end condition
            if(timeInSec <= 0) {
                stopTimer();
            }
        }));
        timer.playFromStart();
    }

    public void formatTime(Label label){
        String hh, mm, ss;
        if(sec < 10) ss = "0" + sec;
        else         ss = String.valueOf(sec);
        if(min < 10) mm = "0" + min;
        else         mm = String.valueOf(min);
        if(hour< 10) hh = "0" + hour;
        else         hh = String.valueOf(hour);
        timeInString = hh + ":" + mm + ":" + ss;
        label.setText(timeInString);
    }

    public static Label formatTime(int timeInSec){
        int hour, min, sec;
        String hh, mm, ss;
        int temp = timeInSec;
        hour = timeInSec / 3600;
        if(hour>99) hour = 0;
        temp = temp - hour * 3600;
        min = temp / 60;
        sec = temp - min * 60;
        if(sec < 10) ss = "0" + sec;
        else         ss = String.valueOf(sec);
        if(min < 10) mm = "0" + min;
        else         mm = String.valueOf(min);
        if(hour< 10) hh = "0" + hour;
        else         hh = String.valueOf(hour);
        return new Label(hh + ":" + mm + ":" + ss);
    }


    //ensure that nothing happens after the time stops
    public synchronized void stopTimer(){
        timer.stop();
    }
    public synchronized void pauseTimer(){
        //pause when pressing save
        timer.pause();
    }
    public void resumeTimer(){
        //resume when save is done or save is canceled
        timer.play();
    }

    public boolean isRunning(){
        return timer.getStatus().equals(Animation.Status.RUNNING);
    }
    public boolean isEnded() {
        if(timeInSec <= 0){
            return true;
        }
        else{
            return false;
        }
    }

    public void resetTimer(){
        hour = 0;
        min = 0;
        sec = 0;
        timeInSec = 0;
        timeInString = "";
    }

    public void convertSecToHourMinSec(int timeInSec){
        this.timeInSec = timeInSec;
        int temp = timeInSec;
        //how many hour are there in total sec?
        //e.g. 7201 / 3600 = 2, in other words, there are 2 hours in 7201 sec
        hour = timeInSec / 3600;
        if(hour>99) hour = 0;
        //get remainder
        temp = temp - hour * 3600;
        //similarly, calculate min
        min = temp / 60;
        //get remainder, note that the remainder is sec
        sec = temp - min * 60;
    }
}
