package data;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * This class serves as a tool for encrypting/decrypting plait text. The
 * encryption algorithm is AES, advanced encryption standard, also known as
 * Rijndael which has been adopted by the U.S. government and is now used
 * worldwide in various fields including finance, trading system, communication,
 * etc. AES has been replacing another wildly used encryption algorithm DES and
 * is now the most secure algorithm. Currently, content encrypted using AES
 * 128-bit key could only be hacked theoretically, thus, using the most advanced
 * super computer would take billions of years, not to mention the encryption
 * using AES 256-bit key.
 *
 * @author Tianyi Lan
 */
public class EncryptionUtils {

    private static final String ALGORITHM = "AES";
    private static final int ENCRYPTION_BIT = 128;
    private static final String CHARACTER_ENCODING = "utf-8";

    /**
     * Encrypt the content using AES algorithm
     *
     * @param content content needs to be encrypted
     * @param userKey user's password as the key
     * @return encrypted content
     */
    public static String encrypt(String content, String userKey) {
        try {
            //create AES key generator
            KeyGenerator generator = KeyGenerator.getInstance(ALGORITHM);

            //use user's password to initialize the key generator
            //note that SecureRandom only constructs a secure random number generator, and password.getBytes() is the seed
            //as long as we are using the same seed, RNG is the same, thus, decryption only requires the same password
            generator.init(ENCRYPTION_BIT, new SecureRandom(userKey.getBytes()));

            //according to user's password, generate a secret key
            SecretKey key = generator.generateKey();

            //return the key in basic encoded format
            byte[] basicEncodedKey = key.getEncoded();

            //covert the key to AES secret key
            SecretKeySpec AES_key = new SecretKeySpec(basicEncodedKey, ALGORITHM);

            //construct cipher
            Cipher cipher = Cipher.getInstance(ALGORITHM);

            //convert content to byte format
            byte[] ContentInByte = content.getBytes(CHARACTER_ENCODING);

            //initialize the cipher for encryption
            cipher.init(Cipher.ENCRYPT_MODE, AES_key);

            //encrypt the content
            byte[] result = cipher.doFinal(ContentInByte);

            return binaryToHex(result);

        } catch (NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException
                | UnsupportedEncodingException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Decrypt the content using AES algorithm
     * Note that decrypting the content using wrong key will produce BadPaddingException,
     * for this app, if this happens, simply catch the exception and return an empty string
     * thus, the decrypted content(password) will not
     *
     * @param content content needs to be decrypted
     * @param userKey user's password as the key
     * @return decrypted content
     */
    public static String decrypt(String content, String userKey) {
        try {
            byte[] binary = hexToBinary(content);
            KeyGenerator generator = KeyGenerator.getInstance(ALGORITHM);
            generator.init(ENCRYPTION_BIT, new SecureRandom(userKey.getBytes()));
            SecretKey key = generator.generateKey();
            byte[] basicEncodedKey = key.getEncoded();
            SecretKeySpec AES_key = new SecretKeySpec(basicEncodedKey, ALGORITHM);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            //now we need the cipher in decrypt mode
            cipher.init(Cipher.DECRYPT_MODE, AES_key);
            assert binary != null;
            byte[] result = cipher.doFinal(binary);
            return new String(result);

        } catch (NoSuchPaddingException | IllegalBlockSizeException
                | InvalidKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //treat BadPaddingException as wrong key, simply return an empty string
        catch ( BadPaddingException e ){
            return "";
        }
        return null;
    }

    //since the encrypted content is in binary format, we need to convert it to hexadecimal
    //therefore, we would store string in hex in files
    private static String binaryToHex(byte binary[]) {
        StringBuilder sb = new StringBuilder();
        for (byte aBinary : binary) {
            String hex = Integer.toHexString(aBinary & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    //convert string in hexadecimal to byte array in binary for decryption
    private static byte[] hexToBinary(String stringInHex) {
        if (stringInHex.length() < 1) {
            return null;
        } else {
            byte[] binary = new byte[stringInHex.length() / 2];
            for (int i = 0; i < stringInHex.length() / 2; i++) {
                int high = Integer.parseInt(stringInHex.substring(i * 2, i * 2 + 1), 16);
                int low = Integer.parseInt(stringInHex.substring(i * 2 + 1, i * 2 + 2), 16);
                binary[i] = (byte) (high * 16 + low);
            }
            return binary;
        }
    }
}