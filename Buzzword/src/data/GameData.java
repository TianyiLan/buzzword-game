package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import propertymanager.PropertyManager;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static buzzword.BuzzwordProperties.*;
import static buzzword.BuzzwordProperties.GAMEMODE_NAME_TOOLTIPS;

/**
 * @author Ritwik Banerjee
 * @author Tianyi Lan
 */
public class GameData implements AppDataComponent {

    //helper inner class to store game progress
    public class GameProgressList{
        public boolean cleared;            //whether or not the level is cleared
        public int wordsDiscovered;        //the number of words discovered
        public int totalScore;             //the total score earned
        GameProgressList(boolean cleared, int wordsDiscovered, int totalScore){
            this.cleared = cleared;
            this.wordsDiscovered = wordsDiscovered;
            this.totalScore = totalScore;
        }
        int size(){return 3;}
    }

    private static final String ANIMAL_FILE = "words/common.txt";
    private static final String PLACES_FILE = "words/places.txt";
    private static final String WORDS_FILE = "words/words.txt";
    private static final String NAMES_FILE = "words/names.txt";

    private String                                username;
    public  Map<String, Integer>                  wordsMap;
    public  Map<String, Integer>                  commonMap;
    public  Map<String, Integer>                  namesMap;
    public  Map<String, Integer>                  placesMap;

    private Map<String, String>                   userInfoMap;
    private Map<Integer, GameProgressList> commonProgressMap;
    private Map<Integer, GameProgressList>        placesProgressMap;
    private Map<Integer, GameProgressList>        celebrityProgressMap;
    private Map<Integer, GameProgressList>        dictionaryWordsProgressMap;
    private GameDataFile                          gamefile;
    public  AppTemplate                           appTemplate;

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            init();
        } else {
            this.appTemplate = appTemplate;
        }
    }

    public void init() {
        username = null;
        gamefile = (GameDataFile) appTemplate.getFileComponent();
        initUserInfoMap();
        initGameModesMaps();
        //preload user info
        preloadUserInfoMap();
        //load all words
        wordsMap = loadWords(WORDS_FILE);    //size = 126k
        commonMap = loadWords(ANIMAL_FILE);  //size = 8.8k
        placesMap = loadWords(PLACES_FILE);  //size = 13.7k
        namesMap = loadWords(NAMES_FILE);    //size = 4.7k

        //show size for testing
        System.out.println("Common list loaded, size: " + commonMap.size());
        System.out.println("Places list loaded, size: " + placesMap.size());
        System.out.println("Names list loaded, size: " + namesMap.size());
        System.out.println("Dic list loaded, size: " + wordsMap.size());

    }

    @Override
    public void reset() {
        username = null;
        resetUserInfoMap();
        resetGameModesMaps();
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getUsername() {
        return username;
    }
    public Map<String, String>  getUserInfoMap() {return userInfoMap;}
    public Map<Integer, GameProgressList> getCommonProgressMap(){return commonProgressMap;}
    public Map<Integer, GameProgressList>  getPlacesProgressMap(){return placesProgressMap;}
    public Map<Integer, GameProgressList>  getCelebrityProgressMap(){return celebrityProgressMap;}
    public Map<Integer, GameProgressList>  getDictionaryWordsProgressMap(){return dictionaryWordsProgressMap;}

    //create a new profile for user
    public boolean createNewProfile(String username, String pw){
        if(userInfoMap.isEmpty() || !userInfoMap.containsKey(username)) {
            this.username = username;
            String encryptedPW = EncryptionUtils.encrypt(pw, pw);
            userInfoMap.put(this.username, encryptedPW);
            gamefile.createProfileFile(this, this.username);
            return true;
        }
        else{
            return false;
        }
    }

    public void changePassword(String pw){
        if(userInfoMap.containsKey(username)){
            String encryptedPW = EncryptionUtils.encrypt(pw, pw);
            userInfoMap.put(this.username, encryptedPW);
            gamefile.updateUserInfo(this);
        }
    }

    //delete the current profile
    public void deleteCurrentProfile(){
        userInfoMap.remove(username);
        resetGameModesMaps();
        try {
            gamefile.deleteProfileFile(this, username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        username = null;
    }

    //login method
    public boolean login(String username, String pw){
        //user info file does not exists, thus user info map is empty
        if(userInfoMap.isEmpty()){
            return false;
        }
        //user info map is not empty
        else {
            //user info map contains username
            if (userInfoMap.containsKey(username)) {
                String storedPW = userInfoMap.get(username);
                String encryptedPW = EncryptionUtils.encrypt(pw, pw);
                //password matches
                if (storedPW.equals(encryptedPW)) {
                    this.username = username;
                    try {
                        gamefile.loadGameProgress(this, username);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                //password does not match
                else{
                    return false;
                }
            }
            //user info map does not contain username
            else {
                return false;
            }
        }
    }

    //logout method
    public void logout(){
        username = null;
        resetGameModesMaps();
    }

    //update the user info
    public void updateUserInfoMap(String username, String pw){
        userInfoMap.put(username, pw);
    }

    public void updateGameProgressWhenLevelCleared(String gameMode, int level, boolean state, int wordsDiscovered, int totalScore){
        updateGameProgress(gameMode,level,state,wordsDiscovered,totalScore);
        gamefile.saveGameProgress(this, username);
    }

    //update the game progress
    public void updateGameProgress(String gameMode, int level, boolean state, int wordsDiscovered, int totalScore) throws IllegalArgumentException{
        PropertyManager propertyManager = PropertyManager.getManager();
        if(gameMode.equals(propertyManager.getPropertyValue(GAMEMODE_COMMON_TOOLTIPS))){
            commonProgressMap.get(level).cleared = state;
            commonProgressMap.get(level).wordsDiscovered = wordsDiscovered;
            commonProgressMap.get(level).totalScore = totalScore;
        }
        else if(gameMode.equals(propertyManager.getPropertyValue(GAMEMODE_PLACES_TOOLTIPS))){
            placesProgressMap.get(level).cleared = state;
            placesProgressMap.get(level).wordsDiscovered = wordsDiscovered;
            placesProgressMap.get(level).totalScore = totalScore;
        }
        else if(gameMode.equals(propertyManager.getPropertyValue(GAMEMODE_NAME_TOOLTIPS))){
            celebrityProgressMap.get(level).cleared = state;
            celebrityProgressMap.get(level).wordsDiscovered = wordsDiscovered;
            celebrityProgressMap.get(level).totalScore = totalScore;
        }
        else if(gameMode.equals(propertyManager.getPropertyValue(GAMEMODE_DICTIONARY_TOOLTIPS))){
            dictionaryWordsProgressMap.get(level).cleared = state;
            dictionaryWordsProgressMap.get(level).wordsDiscovered = wordsDiscovered;
            dictionaryWordsProgressMap.get(level).totalScore = totalScore;
        }
        else{
            throw new IllegalArgumentException("Couldn't find the game mode");
        }
    }

    //initialize all game modes, with 0 level cleared on each game mode
    private void initGameModesMaps(){
        placesProgressMap = new HashMap<>();
        commonProgressMap = new HashMap<>();
        celebrityProgressMap = new HashMap<>();
        dictionaryWordsProgressMap = new HashMap<>();
        zeroGameModesMaps();
    }

    //reset all game modes, with 0 level cleared on each game mode
    public void resetGameModesMaps(){
        //if not null
        if(placesProgressMap != null && commonProgressMap != null
                && celebrityProgressMap != null && dictionaryWordsProgressMap != null){
            //clear all game modes maps
            placesProgressMap.clear();
            commonProgressMap.clear();
            celebrityProgressMap.clear();
            dictionaryWordsProgressMap.clear();
            //reset
            zeroGameModesMaps();
        }
    }

    private void zeroGameModesMaps(){
        PropertyManager propertyManager = PropertyManager.getManager();
        int levelNum = Integer.valueOf(propertyManager.getPropertyValue(NUMBER_LEVELS_IN_EACH_MODE));
        for(int i=0; i<levelNum; i++) {
            placesProgressMap.put(i+1, new GameProgressList(false, 0, 0));
            commonProgressMap.put(i+1, new GameProgressList(false, 0, 0));
            celebrityProgressMap.put(i+1, new GameProgressList(false, 0, 0));
            dictionaryWordsProgressMap.put(i+1, new GameProgressList(false, 0, 0));
        }
    }

    //initialize the user info map
    private void initUserInfoMap(){
        userInfoMap = new HashMap<>();
    }
    //reset the user info map
    public void resetUserInfoMap(){
        if(userInfoMap != null){
            userInfoMap.clear();
        }
    }
    //pre-load user info map
    private void preloadUserInfoMap(){
        if(gamefile.userInfoFileExists()){
            try {
                gamefile.loadUserInfo(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //get game progress info from maps
    public boolean getIsCleared(Map<Integer, GameProgressList> gameProgressMap, int level){
        return gameProgressMap.get(level).cleared;
    }
    public int getWordsDiscovered(Map<Integer, GameProgressList> gameProgressMap, int level){
        return gameProgressMap.get(level).wordsDiscovered;
    }
    public int getTotalScore(Map<Integer, GameProgressList> gameProgressMap, int level){
        return gameProgressMap.get(level).totalScore;
    }


    //load words
    private HashMap<String, Integer> loadWords(String fileName) {
        HashMap<String, Integer> map = new HashMap<>();
        URL wordsResource = getClass().getClassLoader().getResource(fileName);
        assert wordsResource != null;
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            lines.forEach(testTargetString -> {
                testTargetString = testTargetString.toLowerCase();
                if(testTargetString.matches("[a-z]+")){
                    if (testTargetString.length() == 4) {
                        map.put(testTargetString, 20);
                    }
                    if (testTargetString.length() == 5) {
                        map.put(testTargetString, 25);
                    }
                    if (testTargetString.length() == 6) {
                        map.put(testTargetString, 30);
                    }
                    if (testTargetString.length() == 7) {
                        map.put(testTargetString, 35);
                    }
                    if (testTargetString.length() > 7) {
                        map.put(testTargetString, 40);
                    }
                }
            });
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return map;
    }
}