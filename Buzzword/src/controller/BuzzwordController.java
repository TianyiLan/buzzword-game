package controller;

import apptemplate.AppTemplate;
import data.GameData;
import data.GameDataFile;
import data.GameTimer;
import gui.*;
import javafx.animation.AnimationTimer;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Rectangle;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import static buzzword.BuzzwordProperties.*;
import static gui.Vertex.DEFAULT_STROKE_COLOR;
import static gui.Vertex.HIGHLIGHT_COLOR;
import static settings.AppPropertyType.*;

/**
 * @author Tianyi Lan
 */
public class BuzzwordController implements FileController {

    //four states of the game
    public enum GameState {
        NOT_LOGIN,
        LOGIN_NOT_STARTED,
        LOGIN_STARTED,
        ENDED
    }

    private static final int MIN_SOLUTIONS = 6;

    private AppGUI               gui;                        // shared reference to the gui component
    private AppTemplate          appTemplate;                // shared reference to the application
    private Workspace            workspace;                  // shared reference to the workspace
    public  GameData             gamedata;                   // shared reference to the game being played, loaded or saved
    private GameState            gamestate;                  // the state of the game being shown in the workspace
    private GameDataFile         gamefile;                   // shared reference to the game file
    private LevelSelectionScreen levelscreen;                // the screen to select levels and view profile
    private String               selGameMode;                // the selected game mode to play
    private String               guess;
    private Graph                grid;
    private Label                guessLabel;
    private Label                scoreLabel;
    private Label                targetScoreLabel;
    private int                  selLevel;         // the selected level to play
    private Path                 workFile;         // the path of the current player's game progress file
    private Map<String, Integer> minSolution;      // data structure that stores the minimal solutions(words)
    private Map<String, Integer> alreadyGuessed;
    private boolean              isFrozen;         // whether or not the game is paused
    private Button               gameButton;       // shared reference to the "start game" button
    private GameTimer            gameTimer;        // shared reference to the game timer
    private Map<String, Integer> wordsMap;         // shared reference to the word list map
    private final ObservableList<Entry> tableData; //data structure to store game progress
           


    public BuzzwordController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public BuzzwordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.NOT_LOGIN;
        tableData = FXCollections.observableArrayList();
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }
    public GameState getGameState() {
        return this.gamestate;
    }

    /**
     * Method to start the game
     */
    public void start(){
        //first solve the grid and generate possible words
        PropertyManager pm = PropertyManager.getManager();
        alreadyGuessed.clear();
        guessLabel = workspace.getGuessLabel();
        scoreLabel = workspace.getTotalScoreLabel();
        targetScoreLabel = workspace.getTargetScoreLabel();
        grid = workspace.getGraph();
        //load corresponding words map
        if(selGameMode.equals(pm.getPropertyValue(GAMEMODE_COMMON_TOOLTIPS))){
            wordsMap = gamedata.commonMap;
        }
        if(selGameMode.equals(pm.getPropertyValue(GAMEMODE_DICTIONARY_TOOLTIPS))){
            wordsMap = gamedata.wordsMap;
        }
        if(selGameMode.equals(pm.getPropertyValue(GAMEMODE_PLACES_TOOLTIPS))){
            wordsMap = gamedata.placesMap;
        }
        if(selGameMode.equals(pm.getPropertyValue(GAMEMODE_NAME_TOOLTIPS))){
            wordsMap = gamedata.namesMap;
        }

        //solve the grid, re-generate the grid if necessary
        minSolution = solveGrid(wordsMap);
        //clear table
        clearGameProgressTable();

        //set timer
        gameTimer = new GameTimer();
        gameTimer.startCountdownTimer(calTime(selLevel+1), workspace.getTimerLabel());
        isFrozen = false;

        //set graph handler
        setGraphOnInteraction();

        //show solution for testing
        System.out.println(minSolution.keySet());


        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (gameTimer.isEnded()) {
                    stop();
                }

            }
            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void end(){
        setGameState(GameState.ENDED);
        grid.clear();
        disableGraphOnInteraction();
        workspace.updateToolbar(gamestate);
        MessageDialog dialog = new MessageDialog();
        String sol = formatSolution(minSolution);
        //not cleared
        if(Integer.valueOf(scoreLabel.getText()) < Integer.valueOf(targetScoreLabel.getText())){
            workspace.getNextButton().setDisable(true);
            try {
                dialog.setImage(BUZZWORD_ICON.toString(), FAIL_IMAGE.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                dialog.showMSG("Level Not Cleared", sol);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //cleared
        else{
            if(selLevel+1 == 8){
                workspace.getNextButton().setDisable(true);
            }
            try {
                dialog.setImage(BUZZWORD_ICON.toString(), CHECK_IMAGE.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                //only update new best record
                if(tableData.size() > LevelSelectionScreen.getCurrentBestDiscoveredWords()
                        && Integer.valueOf(scoreLabel.getText()) > LevelSelectionScreen.getCurrentBestTotalScore()) {
                    saveGameProgress();
                    dialog.showMSG("New Best Record", "Word Discovered: " + tableData.size() +
                            "\n" + "Total Score: " + Integer.valueOf(scoreLabel.getText()),sol);
                }
                else{
                    dialog.showMSG("Level Cleared", sol);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /** This method will layout GUI and lead player to profile creation and login page */
    @Override
    public void handleNewRequest() throws IOException {
        workspace = (Workspace) this.appTemplate.getWorkspaceComponent();
        gui = this.appTemplate.getGUI();
        gamedata = (GameData) appTemplate.getDataComponent();
        gamedata.init();
        gamefile = (GameDataFile) appTemplate.getFileComponent();
        workspace.layoutGUI();
        alreadyGuessed = new HashMap<>();
        setOnShortKey();
    }

    /** This method will lead player to game play page */
    public void handleStartGameRequest(){
        setGameState(GameState.LOGIN_STARTED);
        workspace.layoutPlayScreen(LevelSelectionScreen.getGameModeSelected(), LevelSelectionScreen.getLevelSelected());
        selGameMode = LevelSelectionScreen.getGameModeSelected();
        selLevel = LevelSelectionScreen.getLevelSelected();
        workspace.updateToolbar(gamestate);
        this.start();
    }

    /** This method will save game progress to current player's profile */
    @Override
    public void handleSaveRequest() throws IOException {
    }

    /** This method will load player's profile with logged in username */
    @Override
    public void handleLoadRequest() throws IOException {
    }

    /** This method will pause the game */
    public void handlePauseRequest(){
        Button resume = workspace.getResumeButton();
        gui.getToolbarPane().getChildren().set(1, resume);
        disableGraphOnInteraction();
        freezeGame(true);
        isFrozen = true;
    }

    /** This method will resume the game */
    public void  handleResumeRequest(){
        Button pause = workspace.getPauseButton();
        gui.getToolbarPane().getChildren().set(1,pause);
        setGraphOnInteraction();
        freezeGame(false);
        isFrozen = false;
    }

    /** This method will have current player logout and return to home page */
    public void handleLogoutRequest() throws IOException {
        YesNoDialog dialog = new YesNoDialog();
        if(gamestate == GameState.LOGIN_STARTED) {
            if(!isFrozen){
                disableGraphOnInteraction();
                freezeGame(true);
            }
        }
        if(dialog.show(CONFIRMATION_LOGOUT.toString())){
            if(gamestate == GameState.LOGIN_STARTED) {
                setGameState(GameState.NOT_LOGIN);
                gamedata.logout();
                workspace.reInitGUI();
                gameTimer.stopTimer();
                disableGraphOnInteraction();
                workspace.updateToolbar(gamestate);
                isFrozen = false;
                //more code
            }
            else{
                setGameState(GameState.NOT_LOGIN);
                gamedata.logout();
                workspace.reInitGUI();
                workspace.updateToolbar(gamestate);
            }
        }
        else{
            if(gamestate == GameState.LOGIN_STARTED) {
                if(!isFrozen){
                    setGraphOnInteraction();
                    freezeGame(false);
                }
            }
        }
    }

    /** This method will have current player return to his/her profile page, WITHOUT logging out */
    public void handlerToProfilePageRequest() throws IOException {
        YesNoDialog dialog = new YesNoDialog();
        if(gamestate == GameState.LOGIN_STARTED) {
            if(!isFrozen) {
                disableGraphOnInteraction();
                freezeGame(true);
            }
        }
        if(dialog.show(CONFIRMATION_PROFILE.toString())){
            //more code
            gameTimer.stopTimer();
            disableGraphOnInteraction();
            setGameState(GameState.LOGIN_NOT_STARTED);
            levelscreen.reset();
            gui.getAppPane().setCenter(levelscreen);
            workspace.updateToolbar(gamestate);
            isFrozen = false;
        }
        else{
            if(gamestate == GameState.LOGIN_STARTED) {
                if(!isFrozen) {
                    setGraphOnInteraction();
                    freezeGame(false);
                }
            }
        }
    }

    /** This method will show an option dialog, allowing player to adjust certain settings */
    @Override
    public void handleOptionsRequest() {

    }

    /** This method will allow players to change their password */
    public void handleChangePWRequest() throws IOException {
        ChangePWDialog dialog = new ChangePWDialog(gamedata);
        dialog.show();
    }

    /** This method will exit the program */
    @Override
    public void handleExitRequest() throws IOException {
        YesNoDialog dialog = new YesNoDialog();
        if(gamestate == GameState.LOGIN_STARTED) {
            if(!isFrozen) {
                disableGraphOnInteraction();
                freezeGame(true);
            }
            if (dialog.show(CONFIRMATION_EXIT.toString())) {
                System.exit(0);
            } else {
                if(!isFrozen) {
                    setGraphOnInteraction();
                    freezeGame(false);
                }
            }
        }
        else{
            if (dialog.show(CONFIRMATION_EXIT.toString())) {
                System.exit(0);
            }
        }
    }

    /**
     * This method will show help information to player regarding how to play the game.
     * Note that in XML, tag "&#xD;" will give a new line.
     * */
    @SuppressWarnings("Duplicates")
    public void handleProfileCreationRequest() throws IOException {
        UserInfoDialog profileDialog = new UserInfoDialog(UserInfoDialog.DialogType.PROFILE_CREATION, gamedata);
        if(profileDialog.show()){
            setGameState(GameState.LOGIN_NOT_STARTED);
            levelscreen = new LevelSelectionScreen(gamedata, workspace);
            gui.getAppPane().setCenter(levelscreen);
            workspace.updateToolbar(gamestate);
        }
    }

    /** This method will process player's login information and lead player to level selection page*/
    @SuppressWarnings("Duplicates")
    public void handleLoginRequest() throws IOException {
        UserInfoDialog loginDialog = new UserInfoDialog(UserInfoDialog.DialogType.LOGIN, gamedata);
        if(loginDialog.show()) {
            setGameState(GameState.LOGIN_NOT_STARTED);
            levelscreen = new LevelSelectionScreen(gamedata, workspace);
            gui.getAppPane().setCenter(levelscreen);
            workspace.updateToolbar(gamestate);
        }
    }

    /** This method will allow player to replay the current level */
    public void  handleReplayRequest() throws IOException {
        YesNoDialog dialog = new YesNoDialog();
        if(gamestate == GameState.LOGIN_STARTED) {
            if(!isFrozen) {
                disableGraphOnInteraction();
                freezeGame(true);
            }
            if(dialog.show(CONFIRMATION_REPLAY.toString())){
                //more code
                workspace.layoutPlayScreen(LevelSelectionScreen.getGameModeSelected(), LevelSelectionScreen.getLevelSelected());
                selGameMode = LevelSelectionScreen.getGameModeSelected();
                selLevel = LevelSelectionScreen.getLevelSelected();
                workspace.updateToolbar(gamestate);
                clearGameProgressTable();
                this.start();
            }
            else{
                if(!isFrozen) {
                    setGraphOnInteraction();
                    freezeGame(false);
                }
            }
        }
        else{
            if(dialog.show(CONFIRMATION_REPLAY.toString())){
                //more code
                setGameState(GameState.LOGIN_STARTED);
                workspace.layoutPlayScreen(LevelSelectionScreen.getGameModeSelected(), LevelSelectionScreen.getLevelSelected());
                selGameMode = LevelSelectionScreen.getGameModeSelected();
                selLevel = LevelSelectionScreen.getLevelSelected();
                workspace.updateToolbar(gamestate);
                clearGameProgressTable();
                this.start();
            }
        }
    }

    /** This method will show some help information */
    public void  handleHelpRequest() throws IOException {
        MessageDialog dialog = new MessageDialog();
        dialog.setImage(BUZZWORD_ICON.toString(), INFO_IMAGE.toString());
        if(gamestate == GameState.LOGIN_STARTED) {
            if(!isFrozen) {
                disableGraphOnInteraction();
                freezeGame(true);
            }
            dialog.show(HELP_TITLE.toString(), HELP_MESSAGE.toString());
            if (!dialog.isShowing()) {
                if(!isFrozen) {
                    setGraphOnInteraction();
                    freezeGame(false);
                }
            }
        }
        else{
            dialog.show(HELP_TITLE.toString(), HELP_MESSAGE.toString());
        }
    }

    /** This method will allow player to delete the current profile */
    public void handleDeleteRequest() throws IOException {
        YesNoDialog dialog = new YesNoDialog();
        if(dialog.show(CONFIRMATION_DELETE.toString())){
            gamedata.deleteCurrentProfile();
            //more code
            setGameState(GameState.NOT_LOGIN);
            workspace.reInitGUI();
            workspace.updateToolbar(gamestate);
        }
    }

    /** This method will allow player to play the next level */
    public void handleNextRequest(){
        LevelSelectionScreen.nextLevelSelected();
        workspace.layoutPlayScreen(LevelSelectionScreen.getGameModeSelected(), LevelSelectionScreen.getLevelSelected());
        selGameMode = LevelSelectionScreen.getGameModeSelected();
        selLevel = LevelSelectionScreen.getLevelSelected();
        workspace.updateToolbar(gamestate);
        clearGameProgressTable();
        this.start();
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private Map<String, Integer> solveGrid(Map<String, Integer> map){
        boolean badGrid = true;
        Map<String, Integer> temp = new HashMap<>();
        while(badGrid){
            temp = grid.solve(map);
            if(temp.size() < MIN_SOLUTIONS){
                workspace.getGraph().reGenerate();
            }
            else{
                badGrid = false;
            }
        }
        return temp;
    }

    public int calTime(int level){
        PropertyManager propertyManager  = PropertyManager.getManager();
        int maxLevel = Integer.valueOf(propertyManager.getPropertyValue(NUMBER_LEVELS_IN_EACH_MODE));
        int baseTime = Integer.valueOf(propertyManager.getPropertyValue(BASE_TIME));
        if(level > maxLevel || level <= 0){
            throw new IllegalArgumentException("Level must be non-zero and no more than "+ maxLevel);
        }
        else{
            return baseTime - ((level) / 2)*10;
        }
    }

    @SuppressWarnings("Duplicates")
    private void setOnShortKey(){
        gui.getPrimaryScene().setOnKeyTyped(null);
        gui.getPrimaryScene().setOnKeyPressed(ke -> {
            //exit = Ctrl + Q
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.Q)){
                try {
                    handleExitRequest();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //logout = Ctrl + L
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.L)){
                if(gamestate == GameState.LOGIN_NOT_STARTED ||
                        gamestate == GameState.LOGIN_STARTED ||
                        gamestate == GameState.ENDED) {
                    try {
                        handleLogoutRequest();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            //to profile page = Ctrl + P
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.P)){
                if(gamestate == GameState.LOGIN_STARTED ||
                        gamestate == GameState.ENDED) {
                    try {
                        handlerToProfilePageRequest();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setGraphOnInteraction(){
        handleMouseDragging();
        handleKeyboardTyping();
    }

    @SuppressWarnings("Duplicates")
    private void disableGraphOnInteraction(){
        //mouse
        for(Vertex v: workspace.getGraph().getVertices()){
            v.setOnDragDetected(null);
            v.setOnMouseDragEntered(null);
            v.setOnMouseEntered(null);
            v.setOnMouseExited(null);
            v.setOnMouseReleased(null);
        }
        //keyboard
        gui.getPrimaryScene().setOnKeyTyped(null);
        gui.getPrimaryScene().setOnKeyPressed(ke -> {
            //exit = Ctrl + Q
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.Q)){
                try {
                    handleExitRequest();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //logout = Ctrl + L
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.L)){
                if(gamestate == GameState.LOGIN_NOT_STARTED ||
                        gamestate == GameState.LOGIN_STARTED ||
                        gamestate == GameState.ENDED) {
                    try {
                        handleLogoutRequest();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            //to profile page = Ctrl + P
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.P)){
                if(gamestate == GameState.LOGIN_STARTED ||
                        gamestate == GameState.ENDED) {
                    try {
                        handlerToProfilePageRequest();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void handleMouseDragging(){
        for(Vertex v: workspace.getGraph().getVertices()){
            //drag started
            v.setOnDragDetected(e -> {
                v.startFullDrag();
                v.setHighlight(true);
                if (!v.isMarked()) {
                    guess = v.getLetter();
                    guessLabel.setText(guess);
                }
                v.setMarked(true);
                v.setPathHead(true);
            });
            //drag entered
            v.setOnMouseDragEntered(e -> {
                if (v.isReachableFromPathHead() && !v.isMarked()) {
                    guess += v.getLetter();
                    guessLabel.setText(guess);
                    v.setHighlight(true);
                    v.setMarked(true);
                    v.highlightPathToHead();
                }
            });
            //mouse entered
            v.setOnMouseEntered(e -> ((Rectangle) v.getChildren().get(0)).setStroke(HIGHLIGHT_COLOR));
            //mouse exited
            v.setOnMouseExited(e -> ((Rectangle) v.getChildren().get(0)).setStroke(DEFAULT_STROKE_COLOR));
            //drag released
            v.setOnMouseReleased(e -> {
                if(!alreadyGuessed.containsKey(guess)) {
                    if (wordsMap.containsKey(guess)) {
                        alreadyGuessed.put(guess, wordsMap.get(guess));
                        updateGameProgress(guess);
                    }
                }
                //reset everything for next guess
                grid.clear();
                guess = "";
                guessLabel.setText(guess);
            });
        }
    }

    @SuppressWarnings("Duplicates")
    private void handleKeyboardTyping(){
        guess = "";
        gui.getPrimaryScene().setOnKeyTyped(e -> {
            String key = e.getCharacter().toLowerCase();
            if (key.matches("[a-z]+")) {
                guess += key;
                guessLabel.setText(guess);
                //first letter input
                if (guess.length() == 1) {
                    //if letter in the grid, proceed
                    if (grid.contains(key)) {
                        grid.findPaths(key);
                        grid.removeInvalidPaths(guess);
                        grid.highlightAllPaths();
                    }
                    //if letter not in the grid, clear gird
                    else {
                        guess = "";
                        guessLabel.setText(guess);
                        grid.clear();
                        grid.clearPath();
                    }
                }
                //rest letter input
                else {
                    //if letter in the grid, proceed
                    if(grid.contains(key)) {
                        grid.findPaths(key);
                        grid.removeInvalidPaths(guess);
                        grid.highlightAllPaths();
                        if (grid.hasNoPath()) {
                            guess = "";
                            guessLabel.setText(guess);
                            grid.clear();
                            grid.clearPath();
                        }
                    }
                    //if letter not in the grid, clear gird
                    else{
                        guess = "";
                        guessLabel.setText(guess);
                        grid.clear();
                        grid.clearPath();
                    }
                }
            }
        });

        //detect ENTER
        gui.getPrimaryScene().setOnKeyPressed(ke -> {
            if (ke.getCode().equals(KeyCode.ENTER)) {
                if (!alreadyGuessed.containsKey(guess)) {
                    if (wordsMap.containsKey(guess)) {
                        alreadyGuessed.put(guess, wordsMap.get(guess));
                        updateGameProgress(guess);
                    }
                }
                //reset everything for next guess
                grid.clear();
                grid.clearPath();
                guess = "";
                guessLabel.setText(guess);
            }

            //exit = Ctrl + Q
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.Q)){
                try {
                    handleExitRequest();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //logout = Ctrl + L
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.L)){
                if(gamestate == GameState.LOGIN_NOT_STARTED ||
                        gamestate == GameState.LOGIN_STARTED ||
                        gamestate == GameState.ENDED) {
                    try {
                        handleLogoutRequest();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            //to profile page = Ctrl + P
            if(ke.isControlDown() && ke.getCode().equals(KeyCode.P)){
                if(gamestate == GameState.LOGIN_STARTED ||
                        gamestate == GameState.ENDED) {
                    try {
                        handlerToProfilePageRequest();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        });


    }

    private void freezeGame(boolean state){
        workspace.hideLetterGird(state);
        if(state) {
            gameTimer.pauseTimer();
        }
        else{
            gameTimer.resumeTimer();
        }
    }

    private void updateGameProgress(String guess){
        if(!wordsMap.containsKey(guess)){
            throw new IllegalArgumentException("Invalid word!");
        }
        else {
            tableData.add(new Entry(guess, wordsMap.get(guess)));
            int temp = Integer.valueOf(scoreLabel.getText());
            scoreLabel.setText(String.valueOf(temp + wordsMap.get(guess)));
        }
    }

    private void clearGameProgressTable(){
        tableData.clear();
    }

    private void saveGameProgress(){
        gamedata.updateGameProgressWhenLevelCleared(
                selGameMode, selLevel+1, true, tableData.size(), Integer.valueOf(scoreLabel.getText()));
        levelscreen.update();
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.LOGIN_NOT_STARTED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.LOGIN_NOT_STARTED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gamedata = (GameData) appTemplate.getDataComponent();
    }

    private String formatSolution(Map<String, Integer> solution){
        String temp = "Complete Solution: " + "\n\n";
        for(String s: solution.keySet()){
                temp += s + "    ";
        }
        return temp;
    }

    public ObservableList<Entry> getTableData(){return tableData;}

    /** Helper inner class as data model for the game progress table */
    public static class Entry {
        private final SimpleStringProperty word;
        private final SimpleStringProperty score;

        private Entry(String word, String score) {
            this.word = new SimpleStringProperty(word);
            this.score = new SimpleStringProperty(score);
        }

        private Entry(String word, int score) {
            this(word, String.valueOf(score));
        }

        public String getWord() {
            return word.get();
        }

        public SimpleStringProperty getWordProp() {
            return word;
        }

        public int getScore() {
            return Integer.valueOf(score.get());
        }

        public SimpleStringProperty getScoreProp() {
            return score;
        }
    }
}