package ui;

import apptemplate.AppTemplate;
import components.AppStyleArbiter;
import controller.FileController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * This class provides the basic user interface for this application, including all the file controls, but it does not
 * include the workspace, which should be customizable and application dependent.
 *
 * @author Richard McKenna, Ritwik Banerjee, Tianyi Lan
 */
public class AppGUI implements AppStyleArbiter {

    protected FileController fileController;   // to react to file-related controls
    protected Stage          primaryStage;     // the application window
    protected Scene          primaryScene;     // the scene graph
    protected BorderPane     appPane;          // the root node in the scene graph, to organize the containers
    protected HBox           toolbarPane;      // the bottom toolbar
    protected Button         newButton;        // button to create a new instance of the application
    protected Button         saveButton;       // button to save progress on application
    protected Button         loadButton;       // button to load a saved game from (json) file
    protected Button         exitButton;       // button to exit application
    protected Button         optionsButton;    // button to adjust settings
    protected String         applicationTitle; // the application title

    private int appWindowWidth;  // optional parameter for window width that can be set by the application
    private int appWindowHeight; // optional parameter for window height that can be set by the application

    /**
     * This constructor initializes the file toolbar for use.
     *
     * @param initPrimaryStage The window for this application.
     * @param initAppTitle     The title of this application, which
     *                         will appear in the window bar.
     * @param app              The app within this gui is used.
     */
    public AppGUI(Stage initPrimaryStage, String initAppTitle, AppTemplate app) throws IOException, InstantiationException {
        this(initPrimaryStage, initAppTitle, app, -1, -1);
    }

    public AppGUI(Stage primaryStage, String applicationTitle, AppTemplate appTemplate, int appWindowWidth, int appWindowHeight) throws IOException, InstantiationException {
        this.appWindowWidth = appWindowWidth;
        this.appWindowHeight = appWindowHeight;
        this.primaryStage = primaryStage;
        this.applicationTitle = applicationTitle;
        initializeToolbar();                    // initialize the top toolbar
        initializeToolbarHandlers(appTemplate); // set the toolbar button handlers
        initializeWindow();                     // start the app window (without the application-specific workspace)
        initializeWindowCloseButtonXHandler();  // set the window close button X handler
    }

    public FileController getFileController() {
        return this.fileController;
    }

    public HBox getToolbarPane() { return toolbarPane; }

    public BorderPane getAppPane() { return appPane; }

    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     *
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }

    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     *
     * @return This application's primary stage (i.e. window).
     */
    public Stage getWindow() { return primaryStage; }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initializeToolbar() throws IOException {
        toolbarPane = new HBox();
        toolbarPane.setAlignment(Pos.CENTER);
        toolbarPane.setSpacing(5);
        toolbarPane.setPadding(new Insets(5,5,5,5));
        newButton = initializeChildButton(toolbarPane, NEW_ICON.toString(), NEW_TOOLTIP.toString(), false, false);
        optionsButton = initializeChildButton(toolbarPane, OPTIONS_ICON.toString(), OPTIONS_TOOLTIP.toString(), false, false);
        //loadButton = initializeChildButton(toolbarPane, LOAD_ICON.toString(), LOAD_TOOLTIP.toString(), false, false);
        //saveButton = initializeChildButton(toolbarPane, SAVE_ICON.toString(), SAVE_TOOLTIP.toString(), true, false);
        exitButton = initializeChildButton(toolbarPane, EXIT_ICON.toString(), EXIT_TOOLTIP.toString(), false, false);

    }

    private void initializeToolbarHandlers(AppTemplate app) throws InstantiationException {
        try {
            Method         getFileControllerClassMethod = app.getClass().getMethod("getFileControllerClass");
            String         fileControllerClassName      = (String) getFileControllerClassMethod.invoke(app);
            Class<?>       klass                        = Class.forName("controller." + fileControllerClassName);
            Constructor<?> constructor                  = klass.getConstructor(AppTemplate.class);
            fileController = (FileController) constructor.newInstance(app);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }

        newButton.setOnAction(e -> {
            try {
                fileController.handleNewRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        //we won't use these 2 buttons in this app
        /*
        saveButton.setOnAction(e -> {
            try {
                fileController.handleSaveRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
                System.exit(1);
            }
        });
        loadButton.setOnAction(e -> {
            try {
                fileController.handleLoadRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        */
        optionsButton.setOnAction(e -> {
            fileController.handleOptionsRequest();
        });

        exitButton.setOnAction(e -> {
            try {
                fileController.handleExitRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }

    //show confirmation dialog when close button X is clicked
    private void initializeWindowCloseButtonXHandler(){
        primaryStage.setOnCloseRequest(event -> {
            event.consume();
            try {
                fileController.handleExitRequest();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void updateWorkspaceToolbar(boolean savable) {
        //saveButton.setDisable(!savable);
        newButton.setDisable(false);
        exitButton.setDisable(false);
    }

    private void initializeWindow() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        // SET THE WINDOW TITLE
        primaryStage.setTitle(applicationTitle);

        // add the toolbar to the constructed workspace
        appPane = new BorderPane();
        appPane.setBottom(toolbarPane);
        // add the home page to the workspace
        appPane.setCenter(initializeImageView(HOME_PAGE.toString()));
        primaryScene = appWindowWidth < 1 || appWindowHeight < 1 ? new Scene(appPane)
                : new Scene(appPane,
                appWindowWidth,
                appWindowHeight);

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resrouces folder does not exist.");
        try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
            primaryStage.getIcons().add(new Image(appLogoStream));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    /**
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar.
     *
     * @param toolbarPane Toolbar pane into which to place this button.
     * @param icon        Icon image file name for the button.
     * @param tooltip     Tooltip to appear when the user mouses over the button.
     * @param disabled    true if the button is to start off disabled, false otherwise.
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
    public static Button initializeChildButton(Pane toolbarPane, String icon, String tooltip, boolean disabled, boolean setFocus) throws IOException {
        Button button = initializeChildButton(icon, tooltip, setFocus);
        toolbarPane.getChildren().add(button);
        button.setDisable(disabled);
        return button;
    }

    //overloaded method for building buttons
    public static Button initializeChildButton(String icon, String tooltip, boolean setFocus) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        Button button = new Button();
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            button.setGraphic(new ImageView(buttonImage));
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            button.setTooltip(buttonTooltip);
            button.setFocusTraversable(setFocus);
            button.setStyle("-fx-border-color: transparent");
            button.setOnMouseEntered(e -> button.setStyle("-fx-border-color: orange"));
            button.setOnMouseExited(e -> button.setStyle("-fx-border-color: transparent"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }

    /**
     * This is a public helper method for initializing a simple image view with
     * a selected image.
     *
     * @param image       Image file name for the image view.
     * @return A constructed, fully initialized image view ready to use.
     */
    public static ImageView initializeImageView(String image) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        ImageView imageView = null;
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(image)))) {
            Image imageToLoad = new Image(imgInputStream);
            imageView = new ImageView(imageToLoad);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return imageView;
    }



    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    @Override
    public void initStyle() {
        // currently, we do not provide any stylization at the framework-level
    }
}