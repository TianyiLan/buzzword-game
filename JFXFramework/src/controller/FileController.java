package controller;

import java.io.IOException;

/**
 * @author Ritwik Banerjee
 */
public interface FileController {

    void handleNewRequest() throws IOException;

    void handleSaveRequest() throws IOException;

    void handleLoadRequest() throws IOException;

    void handleOptionsRequest();

    void handleExitRequest() throws IOException;
}
